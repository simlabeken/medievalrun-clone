using System;
using System.Collections;
using Unity.Collections;
using UnityEngine;
using UnityEngine.EventSystems;

public class UIInputManager : MonoBehaviourSingleton<UIInputManager>
{
    public static Action<Vector2> OnMoving;
    public static Action OnMoveJoystickReleased;
    protected bool IsInputActive = true;
    public Joystick joystick;
    //public static Action OnSkillButtonPressed;

    //[Range(0.001f, 0.1f)]
    //[SerializeField] float dataSendRate = .1f;

    [ReadOnly]
    [SerializeField] protected bool isMoving = false;
    [ReadOnly]
    [SerializeField] protected Vector2 currentMovement = Vector2.zero;

    //protected PlayerInput input;

    //#region Unity Methods

    //protected virtual void OnEnable()
    //{
    //    input.CharacterControls.Enable();
    //    GameStateManager.OnGameEnded += OnGameEnded;
    //}

    //protected virtual void OnDisable()
    //{
    //    input.CharacterControls.Disable();
    //    GameStateManager.OnGameEnded -= OnGameEnded;
    //}

    //protected virtual void Awake()
    //{
    //    input = new PlayerInput();

    //    input.CharacterControls.Movement.started += ctx =>
    //    {
    //        //currentMovement = ctx.ReadValue<Vector2>();
    //        isMoving = true;
    //        //Debug.Log("Move: " + currentMovement);
    //    };

    //    input.CharacterControls.Movement.canceled += ctx =>
    //    {
    //        currentMovement = Vector2.zero;
    //        isMoving = false;
    //        OnMoveJoystickReleased?.Invoke();
    //    };
    //}

    private void Update()
    {
        if (joystick.Direction.magnitude > 0.1f)
            isMoving = true;
        else if (isMoving)
        {
            isMoving = false;
            OnMoveJoystickReleased?.Invoke();
        }
    }

    protected virtual void FixedUpdate()
    {
        if (isMoving && IsInputActive)
            OnMoving?.Invoke(joystick.Direction.normalized);
        currentMovement = joystick.Direction;
        //OnMoving?.Invoke(currentMovement);
    }

    //protected virtual void Update()
    //{
    //    if (isMoving)
    //    {
    //        currentMovement = input.CharacterControls.Movement.ReadValue<Vector2>();
    //    }
    //}

    protected void OnGameEnded()
    {
        isMoving = false;
        OnMoveJoystickReleased?.Invoke();
        gameObject.SetActive(false);
    }

    public virtual void StopPlayer()
    {
        currentMovement = Vector2.zero;
        isMoving = false;
        OnMoveJoystickReleased?.Invoke();
        IsInputActive = false;
    }

    public virtual void ContinuePlayer()
    {
        IsInputActive = true;
    }

}