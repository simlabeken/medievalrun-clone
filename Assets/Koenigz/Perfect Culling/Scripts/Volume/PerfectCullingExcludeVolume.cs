﻿// Perfect Culling (C) 2021 Patrick König
//

using System;

#if UNITY_EDITOR
using UnityEditor;
#endif

using UnityEngine;
using UnityEngine.Rendering;

namespace Koenigz.PerfectCulling
{
    public class PerfectCullingExcludeVolume : MonoBehaviour, CustomHandle.IResizableByHandle
    { 
        static readonly Bounds UniformBounds = new Bounds(Vector3.zero, Vector3.one);

        [SerializeField] public Vector3 volumeSize = Vector3.one;
        [SerializeField] public PerfectCullingBakingBehaviour[] restrictToBehaviours = System.Array.Empty<PerfectCullingBakingBehaviour>();

        public Bounds volumeExcludeBounds
        {
            get => new Bounds(transform.position, volumeSize);

            set
            {
                // TODO: Causes annoying offset, gonna need to solve this in a different way.
                transform.position = value.center;
                
                volumeSize = new Vector3(
                    Mathf.Max(1, (value.size.x)), 
                    Mathf.Max(1, (value.size.y)), 
                    Mathf.Max(1, (value.size.z)));
            }
        }

        public bool IsPositionActive(PerfectCullingBakingBehaviour bakingBehaviour, Vector3 pos)
        {
            if (restrictToBehaviours.Length > 0)
            {
                if (System.Array.IndexOf(restrictToBehaviours, bakingBehaviour) < 0)
                {
                    return false;
                }
            }
            
            Matrix4x4 matrix4X4 = Matrix4x4.TRS(transform.position, transform.rotation, volumeExcludeBounds.size).inverse;
            
            return UniformBounds.Contains(matrix4X4.MultiplyPoint3x4(pos));
        }

        public Vector3 HandleSized
        {
            get => volumeExcludeBounds.size;
            set => volumeExcludeBounds = new Bounds(transform.position, value);
        }

#if UNITY_EDITOR
        private void OnDrawGizmosSelected()
        {
            Handles.matrix = transform.localToWorldMatrix;
        
            Handles.zTest = CompareFunction.LessEqual;
            Handles.color = Color.red;
            Handles.DrawWireCube(Vector3.zero, volumeSize);
        }
#endif
    }
}