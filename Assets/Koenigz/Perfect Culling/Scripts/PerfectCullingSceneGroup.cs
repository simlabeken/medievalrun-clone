﻿// Perfect Culling (C) 2021 Patrick König
//

using System.Collections.Generic;
using UnityEngine;

namespace Koenigz.PerfectCulling
{
    public class PerfectCullingSceneGroup : PerfectCullingMonoGroup
    {
        [SerializeField] private Renderer[] renderers;
        
        public override List<Renderer> Renderers
        {
            get
            {
                List<Renderer> rs = new List<Renderer>(renderers);

                rs.RemoveAll((r) => r == null);

                return rs;
            }
        }

        public void SetRenderers(Renderer[] newRenderers)
        {
            renderers = newRenderers;
        }
    }
}