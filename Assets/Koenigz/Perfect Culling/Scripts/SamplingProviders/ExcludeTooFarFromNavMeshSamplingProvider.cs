﻿// Perfect Culling (C) 2021 Patrick König
//

using UnityEngine;
using UnityEngine.AI;

namespace Koenigz.PerfectCulling.SamplingProviders
{ 
    [RequireComponent(typeof(PerfectCullingBakingBehaviour))]
    [DisallowMultipleComponent]
    [ExecuteAlways]
    public class ExcludeTooFarFromNavMeshSamplingProvider : SamplingProviderBase
    {
        [SerializeField] private float distance = 2.5f;
        
        public override string Name => nameof(ExcludeTooFarFromNavMeshSamplingProvider);
      
        public override void InitializeSamplingProvider()
        {
        }

        public override bool IsSamplingPositionActive(PerfectCullingBakingBehaviour bakingBehaviour, Vector3 pos)
        {
            // Just check whether navmesh is within specified distance.
            if (!UnityEngine.AI.NavMesh.SamplePosition(pos, out NavMeshHit navMeshHit, distance, NavMesh.AllAreas))
            {
                return false;
            }
            
            return true;
        }
    }
}