﻿using System;
using UnityEngine;

namespace PhysicsActionSystem
{
    [Serializable]
    public class VelocityCondition 
    {
        Rigidbody rigidbody;

        [SerializeField] bool checkX;
        [SerializeField] bool checkY;
        [SerializeField] bool checkZ;

        [SerializeField] bool checkAbove;
        [SerializeField] float value;

        public void SetRigidbody(Rigidbody rb)
        {
            rigidbody = rb;
        }

        public bool IsConditionRealized()
        {
            if (!rigidbody) return false;
            if (checkX)
            {
                return rigidbody.velocity.x > value == checkAbove;
            }
            else if (checkY)
            {
                return rigidbody.velocity.y > value == checkAbove;
            }
            else if (checkZ)
            {
                return rigidbody.velocity.z > value == checkAbove;
            }
            else return false;
        }
    }

}