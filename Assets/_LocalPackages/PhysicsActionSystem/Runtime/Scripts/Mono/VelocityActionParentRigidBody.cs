﻿using UnityEngine;

namespace PhysicsActionSystem
{
    public class VelocityActionParentRigidBody : VelocityAction //TODO: Add to project template
    {
        protected override void SetRigidbody()
        {
            rb = GetComponentInParent<Rigidbody>();
        }
    }

}