using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace PhysicsActionSystem
{

    public class VelocityAction : MonoBehaviour
    {
        [SerializeField] bool isActionOn=true;
        [SerializeField] Cooldown coolDown;
        [SerializeField] VelocityCondition[] velocityConditions;
        [SerializeField] UnityEvent onConditionRealised;
        [SerializeField] UnityEvent onConditionNotRealised;
        [SerializeField] protected Rigidbody rb;

        void Start()
        {
            if(rb == null)
            SetRigidbody();

            for (int i = 0; i < velocityConditions.Length; i++)
            {
                velocityConditions[i].SetRigidbody(rb);
            }
        }

        protected virtual void SetRigidbody()
        {
            rb = GetComponent<Rigidbody>();
        }

        void Update()
        {
            if (!isActionOn) return;
            if (!coolDown.IsCooldownFinished) return;
            if (IsConditionRealized())
                CallEvent(onConditionRealised);
            else
                CallEvent(onConditionNotRealised);

        }

        bool IsConditionRealized()
        {
            var condition = true;
            for (int i = 0; i < velocityConditions.Length; i++)
            {
                condition = condition && velocityConditions[i].IsConditionRealized();
            }
            return condition;
        }

        void CallEvent(UnityEvent unityEvent)
        {
            if (unityEvent.GetPersistentEventCount() == 0)
            {
                Debug.Log("No listener");
                return;
            }
            unityEvent?.Invoke();
            coolDown.UpdateCooldown();
            //Debug.Log(rigidbody.velocity);
        }

        public void SetAction(bool value)
        {
            isActionOn = value;
        }
    }

}