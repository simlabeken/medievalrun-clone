﻿using UnityEngine;

namespace PhysicsActionSystem
{
    public class VelocityActionChildRigidBody : VelocityAction //TODO: Add to project template
    {
        protected override void SetRigidbody()
        {
            rb = GetComponentInChildren<Rigidbody>();
        }
    }

}