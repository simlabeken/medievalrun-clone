using UnityEngine;

namespace SequenceSystem
{
    [CreateAssetMenu(fileName = "TimeTrigger", menuName = "SequenceSystems/TimeTrigger")]
    public class TimeTrigger : SequenceTrigger
    {
        [SerializeField] float floatTriggerTime;
        [ReadOnly][SerializeField] float startTime;

        public override void OnStart()
        {
            startTime = Time.realtimeSinceStartup;
        }

        public override void OnUpdate()
        {
            if (floatTriggerTime <= Time.realtimeSinceStartup - startTime)
                CallTriggerAction();
        }
    }

}