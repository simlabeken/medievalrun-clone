using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ReplenishableSystem.HealthSystem
{
    public class Health : Replenishable
    {
        public Action OnAgentDead;
        public Action<int, bool> OnHealthValueChanged;

        protected override void SetNewValue(float value)
        {
            SetHealth(value);
        }

        protected void SetHealth(float value)
        {
            CurrentValue = Mathf.Min(value, maxValue);
            OnValueChanged?.Invoke();
            if (CurrentValue == 0)
            {
                OnValueBelowZero?.Invoke();
                TryToDie();
            }
        }

        protected virtual void TryToDie()
        {
            //OnDead();
        }

        //protected void OnDead()
        //{
        //    if (gameObject.CompareTag("Player") && PlayerManager.Instance != null)
        //    {
        //        PlayerManager.Instance.DestroyPlayer();
        //    }

        //    OnAgentDead?.Invoke();
        //    //else if (gameObject.CompareTag("Bot"))
        //    //{
        //    //    BotManager.Instance.DestroyBot(gameObject);
        //    //}
        //    //else if (gameObject.CompareTag("Police"))
        //    //{
        //    //    PoliceManager.Instance.DestroyPolice(gameObject);
        //    //}
        //    //else if (gameObject.CompareTag("Turret"))
        //    //{
        //    //    GetComponent<TurretBot>().OnTurretDead();
        //    //}
        //}

        public void ResetHealth()
        {
            SetNewValue(0);
        }
    }
}

