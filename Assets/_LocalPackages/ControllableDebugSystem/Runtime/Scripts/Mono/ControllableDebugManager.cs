using UnityEngine;

namespace ControllableDebugSystem
{
    public class ControllableDebugManager : MonoBehaviour
    {
        [SerializeField] bool showControllableDebug;
        static ControllableDebugManager Instance;
        void Awake()
        {
            if (Instance)
                Destroy(gameObject);
            else
            {
                Instance = this;
                transform.parent = null;
                DontDestroyOnLoad(gameObject);
                Debug.IsDebugActive = showControllableDebug;
                UnityEngine.Debug.unityLogger.logEnabled = showControllableDebug;

                #if UNITY_IOS
                    Debug.LogWarning("Logs are disabled on iOS");
                    Debug.IsDebugActive = false;
                    UnityEngine.Debug.unityLogger.logEnabled = false;
                #endif
            }
        }

        private void OnValidate()
        {
            Debug.IsDebugActive = showControllableDebug;
        }

    }

}


