﻿using UnityEngine;

namespace ControllableDebugSystem
{
    public class Debug
    {
        public static bool IsDebugActive;

        public static void Log(object message)
        {
            if (IsDebugActive)
                UnityEngine.Debug.Log(message);
        }

        public static void Log(object message, Object context)
        {
            if(IsDebugActive)
                UnityEngine.Debug.Log(message,context);
        }

        public static void LogWarning(object message)
        {
            if (IsDebugActive)
                UnityEngine.Debug.LogWarning(message);
        }

        public static void LogWarning(object message, Object context)
        {
            if (IsDebugActive)
                UnityEngine.Debug.LogWarning(message, context);
        }

        public static void LogError(object message)
        {
            if (IsDebugActive)
                UnityEngine.Debug.LogError(message);
        }

        public static void LogError(object message, Object context)
        {
            if (IsDebugActive)
                UnityEngine.Debug.LogError(message, context);
        }

        public static void DrawLine(Vector3 start, Vector3 end)
        {
            if (IsDebugActive)
                UnityEngine.Debug.DrawLine(start, end );
        }

        public static void DrawLine(Vector3 start, Vector3 end, Color color)
        {
            if (IsDebugActive)
                UnityEngine.Debug.DrawLine(start, end, color);
        }

        public static void DrawLine(Vector3 start, Vector3 end, Color color, float duration)
        {
            if (IsDebugActive)
                UnityEngine.Debug.DrawLine(start, end,color,duration);
        }

        public static void DrawLine(Vector3 start, Vector3 end, [UnityEngine.Internal.DefaultValue("Color.white")] Color color, [UnityEngine.Internal.DefaultValue("0.0f")] float duration, [UnityEngine.Internal.DefaultValue("true")] bool depthTest)
        {
            if (IsDebugActive)
                UnityEngine.Debug.DrawLine(start, end, color, duration,depthTest);
        }

        public static void DrawRay(Vector3 start, Vector3 dir)
        {
            if (IsDebugActive)
                UnityEngine.Debug.DrawRay(start, dir);
        }

        public static void DrawRay(Vector3 start, Vector3 dir, Color color)
        {
            if (IsDebugActive)
                UnityEngine.Debug.DrawRay(start, dir,color);
        }

        public static void DrawRay(Vector3 start, Vector3 dir, Color color, float duration)
        {
            if (IsDebugActive)
                UnityEngine.Debug.DrawRay(start, dir, color,duration);
        }

        public static void DrawRay(Vector3 start, Vector3 dir, [UnityEngine.Internal.DefaultValue("Color.white")] Color color, [UnityEngine.Internal.DefaultValue("0.0f")] float duration, [UnityEngine.Internal.DefaultValue("true")] bool depthTest)
        {
            if (IsDebugActive)
                UnityEngine.Debug.DrawRay(start, dir, color, duration,depthTest);
        }
    }

}


