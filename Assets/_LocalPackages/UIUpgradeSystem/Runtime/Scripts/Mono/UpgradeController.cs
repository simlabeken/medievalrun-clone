using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace UIUpgradeSystem
{
    public abstract class UpgradeController : MonoBehaviour
    {
        [SerializeField] UpgradeRequirement[] upgradeRequirements;
        [ReadOnly][SerializeField]protected int currentUpgradeLevel;
        [SerializeField] string playerPrefsSaveKey;
        [SerializeField] Button upgradeButton;
        public static Action OnAnyUpgrade;


        void Awake()
        {
            LoadLevel();
            OnUpgrade();
        }

        private void OnEnable()
        {
            OnAnyUpgrade += UpdateUI;
        }

        private void OnDisable()
        {
            OnAnyUpgrade -= UpdateUI;
        }

        public void TryUpgrade()
        {
            if (!IsReadyToUpgrade()) return;
            Upgrade();
        }

        public bool IsReadyToUpgrade()
        {
            var isReady = true;
            for (int i = 0; i < upgradeRequirements.Length; i++)
            {
                if (!upgradeRequirements[i].IsRequirementSatisfied(currentUpgradeLevel))
                {
                    isReady = false;
                    break;
                }
            }
            return isReady;
        }

        void Upgrade()
        {
            currentUpgradeLevel++;
            SaveLevel();
            foreach (var upgradeRequirement in upgradeRequirements)
            {
                upgradeRequirement.OnUpgrade(currentUpgradeLevel-1);
                upgradeRequirement.CurrentLevel = currentUpgradeLevel;
                upgradeRequirement.OnUpgraded?.Invoke();
            }
            OnUpgrade();
            OnAnyUpgrade?.Invoke();
        }

        protected abstract void OnUpgrade();

        void SaveLevel()
        {
            if (playerPrefsSaveKey == "") return;
            PlayerPrefs.SetInt(playerPrefsSaveKey, currentUpgradeLevel);
        }

        void LoadLevel()
        {
            if (playerPrefsSaveKey == "") return;
            currentUpgradeLevel = PlayerPrefs.GetInt(playerPrefsSaveKey, 0);
            foreach (var upgradeRequirement in upgradeRequirements)
            {
                upgradeRequirement.CurrentLevel = currentUpgradeLevel;
            }
        }

        public void UpdateUI()
        {
            upgradeButton.interactable = IsReadyToUpgrade();
        }
    }
}