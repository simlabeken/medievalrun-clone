using System;
using UnityEngine;

namespace InputSystem
{

    public class SwipeInputManager : MonoBehaviour
    {
        Vector2 touchStartPos;
        Vector2 touchEndPos;

        float touchStartTime;
        float swipeTimeTreshold = 2;

        float swipeDistanceTreshold = 50;

        public static Action OnswipeDown;
        public static Action OnswipeUp;
        public static Action OnswipeLeft;
        public static Action OnswipeRight;

        void Update()
        {
          //  if (!enabled) return;
            if (Input.GetMouseButtonDown(0))
            {
                touchStartPos = Input.mousePosition;
                touchStartTime = Time.time;
                Debug.Log("down");
            }

            if (Input.GetMouseButtonUp(0))
            {
                if (Time.time - touchStartTime < swipeTimeTreshold)
                {
                    touchEndPos = Input.mousePosition;
                    CalculateSwipe();
                }
            }
        }

        void CalculateSwipe()
        {
            Vector2 swipeVector = touchEndPos - touchStartPos;


            if(Mathf.Abs(swipeVector.y) > Mathf.Abs(swipeVector.x) && Mathf.Abs(swipeVector.y) > swipeDistanceTreshold)
            {
                if (swipeVector.y < 0 )
                {
                    OnswipeDown?.Invoke();
                }
                else
                    OnswipeUp?.Invoke();
            }
            else if (Mathf.Abs(swipeVector.x) >= Mathf.Abs(swipeVector.y) && Mathf.Abs(swipeVector.x) > swipeDistanceTreshold)
            {
                if (swipeVector.x < 0)
                {
                    OnswipeLeft?.Invoke();
                }
                else
                    OnswipeRight?.Invoke();
            }

        }
    }
}
