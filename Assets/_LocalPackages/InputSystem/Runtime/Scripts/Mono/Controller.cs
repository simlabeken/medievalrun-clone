using AgentSystem;
using InputSystem;
using UnityEngine;

public class Controller : MonoBehaviour
{
    [ReadOnly]
    public AgentController AgentController;
    protected InputListener inputListener;

    public virtual void Initialize()
    {
        AgentController = GetComponent<AgentController>();
    }
}