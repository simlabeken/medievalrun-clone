﻿using UnityEngine.Events;

namespace DroppableSystem
{
    public interface ICanDrop
    {
        UnityEvent<IDroppable> OnDropped { get; }
        void Drop(IDroppable IDroppable);
    }
}
