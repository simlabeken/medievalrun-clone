﻿using ScriptableSystem;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace EventsSystem
{

    public class OnStartEvent : MonoBehaviour
    {
        [SerializeField] UnityEvent onStart;

        private void Start()
        {
            onStart?.Invoke();
        }
    }
}