﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace EventsSystem
{
    public class OnUnloadedEvent : SceneEvents
    {
        [SerializeField] UnityEvent onUnload;

        private void OnEnable()
        {
            SceneManager.sceneUnloaded += OnSceneUnloaded;
        }

        private void OnDisable()
        {
            SceneManager.sceneUnloaded -= OnSceneUnloaded;
        }

        void OnSceneUnloaded(Scene scene)
        {
            var name = scene.name;
            Debug.Log(name + " scene is Unloaded");
            if (IsConditionRealised(scene.name))
                onUnload?.Invoke();
        }
    }

}