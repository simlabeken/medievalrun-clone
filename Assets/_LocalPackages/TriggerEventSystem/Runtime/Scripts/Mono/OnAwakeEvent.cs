﻿using UnityEngine;
using UnityEngine.Events;

namespace EventsSystem
{
    public class OnAwakeEvent : MonoBehaviour
    {
        [SerializeField] UnityEvent onAwake;

        private void Awake()
        {
            onAwake?.Invoke();
        }
    }

}