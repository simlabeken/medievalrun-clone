﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ReplenishableSystem
{
    public class ReplenishableUI : MonoBehaviour
    {
        [SerializeField] Replenishable replenishable;
        [SerializeField] bool hasFadeOut;
        [SerializeField] bool isReverseFadeOut;
        [SerializeField]
        [Range(0, 1)]
        float fadeOutStartPercentage = .1f;
        public Replenishable Replenishable
        {
            get { return replenishable; }
            set
            {
                if (replenishable)
                    replenishable.OnValueChanged -= UpdateUI;
                replenishable = value;
                if (replenishable)
                {
                    replenishable.OnValueChanged += UpdateUI;
                    UpdateUI();
                }
            }
        }
        [SerializeField] TMP_Text fullReplenishableText;
        [SerializeField] TMP_Text currentReplenishableText;
        [SerializeField] protected Image backgroundBar;
        [SerializeField] protected Image replenishableBar;
        Vector2 barSize = new Vector2(.015f, .25f);

        int barPerValue = 100;
        bool isBarSet;

        public virtual void UpdateUI()
        {
            if (Replenishable == null)
            {
                replenishableBar.transform.parent.gameObject.SetActive(false);
                return;
            }
            if (fullReplenishableText) fullReplenishableText.text = Replenishable.InitialValue.ToString();
            SetBars(Replenishable.InitialValue);
            if (currentReplenishableText) currentReplenishableText.text = Replenishable.CurrentValue.ToString();
            if (replenishableBar) replenishableBar.fillAmount = Replenishable.ReplenishablePercentage;
            replenishableBar.transform.parent.gameObject.SetActive(Replenishable.CurrentValue > 0);

            if (hasFadeOut)
            {
                var multiplier = 1 / fadeOutStartPercentage;
                var color = replenishableBar.color;
                if (!isReverseFadeOut && Replenishable.ReplenishablePercentage < fadeOutStartPercentage)
                {
                    color.a = Replenishable.ReplenishablePercentage * multiplier;
                }
                else if (isReverseFadeOut && Replenishable.ReplenishablePercentage > (1 - fadeOutStartPercentage))
                {
                    color.a = (1 - Replenishable.ReplenishablePercentage) * multiplier;
                }
                else
                {
                    color.a = 1;
                }

                replenishableBar.color = color;
                if (backgroundBar)
                    backgroundBar.color = color;
            }
        }

        void SetBars(float maxValue)
        {
            if (isBarSet) return;
            float amount = (maxValue / barPerValue) - 1;
            for (int i = 0; i < amount; i++)
            {
                var bar = new GameObject();
                bar.transform.parent = replenishableBar.transform;
                var image = bar.AddComponent<Image>();
                image.color = new Color(0, 0, 0, .5f);
                var rectTransform = bar.GetComponent<RectTransform>();
                rectTransform.sizeDelta = barSize;
                rectTransform.localScale = Vector3.one;
                rectTransform.localPosition = new Vector3((i + 1) * (1f / (amount + 1)) - .5f, 0, 0);
            }

            isBarSet = true;
        }

        public void ToggleUI(bool value)
        {

        }
    }
}