﻿using UnityEngine.Events;

namespace CollectableSystem
{
    public interface IDroppable
    {
        UnityEvent OnDropped { get; }
        bool isDropped { get; }
        void Drop();
    }
}
