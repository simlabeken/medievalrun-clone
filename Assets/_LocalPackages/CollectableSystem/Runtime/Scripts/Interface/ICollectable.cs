using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace CollectableSystem
{
    public interface ICollectable
    {
        UnityEvent OnCollected { get; }
        UnityEvent OnCollectFailed { get; }
        bool IsCollected { get; }
        void GetCollected();
    }
}
