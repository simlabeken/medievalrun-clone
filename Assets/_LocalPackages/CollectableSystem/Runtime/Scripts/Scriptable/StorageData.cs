﻿using System;
using UnityEngine;

namespace CollectableSystem
{
    [CreateAssetMenu(fileName = "StorageData", menuName = "CollectableSystem/Create Storage Data")]
    public class StorageData : ScriptableObject
    {
        [ReadOnly]
        [SerializeField]
        protected float currentValue;
        public float CurrentValue { set { currentValue = Mathf.Min(value, CurrentMaxValue); OnValueChanged?.Invoke(); } }
        [Min(.1f)]
        public float MaxValue;
        [Min(.1f)]
        [ReadOnly]
        [SerializeField]
        float currentMaxValue;
        public float CurrentMaxValue
        {
            get { if (currentMaxValue == 0) currentMaxValue = MaxValue; return currentMaxValue; }
            set { currentMaxValue = value; OnValueChanged?.Invoke(); }
        }
        public float Ratio { get { return currentValue / CurrentMaxValue; } }
        public bool IsFull { get { return currentValue >= CurrentMaxValue; } }
        public Action OnValueChanged;

        private void Awake()
        {
            CurrentMaxValue = MaxValue;
        }

        public virtual float GetCurrentValue()
        {
            return currentValue;
        }
    }
}