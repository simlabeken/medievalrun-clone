using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.SceneManagement;

namespace GameLevelSystem
{
    [CreateAssetMenu(menuName = "Game Level System/Create Level Data")]
    public class LevelData : ScriptableObject
    {
        public const string LevelPath = "Assets/Scenes/Levels/";
        // [SerializeField] int maxPlayerCount;
        // [SerializeField] float gameTime;
        // [SerializeField] GameMode gameMode;
        //[SerializeField] bool canPlayerAttack;

        // public int MaxPlayerCount { get { return maxPlayerCount; } }
        // public float GameTime { get { return gameTime; } }
        // public GameMode GameMode { get { return gameMode; } }
        // public bool CanPlayerAttack { get { return canPlayerAttack; } }
        public List<Vector3> SpawnPositions;
        public string MapName;
        public int SceneIndex
        {
            get
            { 
                Debug.Log("Map name: " + MapName + " Path: " + LevelPath + MapName + ".unity" + " Index: " + SceneUtility.GetBuildIndexByScenePath(LevelPath + MapName + ".unity"));
                return SceneUtility.GetBuildIndexByScenePath(LevelPath + MapName + ".unity"); 
            }
        }
        public int MapIndex;
    }
}