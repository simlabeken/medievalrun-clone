﻿using ScriptableSystem;
using Sirenix.OdinInspector;
using UnityEngine;

namespace SimilaritySystem
{

    public abstract class Similarity
    {
        public abstract float GetSimilarityPercentage();
        [ReadOnly] [SerializeField] float currentSimilarity;
        [SerializeField] protected ScriptableFloat similarityPercentage;

        [Button("Update Similarity Percentage")]
        public void UpdateSimilarityPercentage()
        {
            currentSimilarity = GetSimilarityPercentage();
            similarityPercentage.UpdateValue(currentSimilarity);
        }
    }
}
