﻿using System;
using ScriptableSystem;
using UnityEngine;

namespace SimilaritySystem
{

    [Serializable]
    public class ColorSimilarity : Similarity
    {
        public ScriptableColor CurrentColor;
        public ScriptableColor TargetColor;

        public override float GetSimilarityPercentage()
        {
            if (CurrentColor.IsNull())
            {
                Debug.Log("No current color");
                return 0;
            }
            if (TargetColor.IsNull())
            {
                Debug.Log("No target color");
                return 0;
            }
            return CurrentColor.GetValue() == TargetColor.GetValue() ? 100 : 0;
        }
    }
}
