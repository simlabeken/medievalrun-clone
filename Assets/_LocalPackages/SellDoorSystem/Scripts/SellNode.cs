using System.Collections;
using System.Collections.Generic;
using GroupMovement.ChainMovementSystem;
using ScriptableSystem;
using UnityEngine;
using UnityEngine.Events;

public class SellNode : MonoBehaviour
{
    [SerializeField] ScriptableInt totalMoney;
    [SerializeField] UnityEvent<GameObject> onSell;

    public void Sell(GameObject go)
    {
        var itemPriceController = go.GetComponentInChildren<AddonPriceController>();
        var node = go.GetComponentInChildren<ChainNode>();
        if (!itemPriceController.IsNull(true) && !node.IsNull(true) && node.NodeController && !node.IsLeader())
        {
            // node.ChainController.RemoveNode(node);
            // node.transform.parent = null;
            // totalMoney.IncreaseValue(shoePriceController.GetCurrentPrice());
            // onSell?.Invoke(go);
            if (!node.IsLeader())
            {
                node.NodeController.RemoveNode(node);
                // node.RemoveFromChainController(true);
                node.transform.parent = null;
                totalMoney.IncreaseValue(itemPriceController.GetCurrentPrice());
                onSell?.Invoke(go);
            }
        }
    }

}