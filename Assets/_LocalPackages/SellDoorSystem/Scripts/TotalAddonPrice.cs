using AddonSystem;
using GroupMovement.ChainMovementSystem;
using ScriptableSystem;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TotalAddonPrice : MonoBehaviour
{

    [SerializeField] ChainController chainController;
    ChainController chainControllerGetter
    {
        get
        {
            if (chainController.IsNull())
                chainController = GameObject.FindObjectOfType<ChainController>();
            return chainController;

        }

    }
    [SerializeField] ScriptableInt totalPrice;


    private void OnEnable()
    {
        AddonController.OnAddonUpdated += UpdatePrice;
        InvokeRepeating(nameof(SetChainControllerSetter),0,.5f);
    }

    private void SetChainControllerSetter()
    {
        if (chainControllerGetter.IsNull(true)) return;
        chainControllerGetter.OnNodeListUpdated += UpdatePrice;
        CancelInvoke();
    }

    private void OnDisable()
    {
        AddonController.OnAddonUpdated -= UpdatePrice;
        chainControllerGetter.OnNodeListUpdated -= UpdatePrice;
    }


    public int GetTotalPrice()
    {
        var total = 0;
        for (int i = 0; i < chainControllerGetter.NodeCount; i++)
        {
            var priceController = chainControllerGetter.Nodes[i].GetComponentInChildren<AddonPriceController>();
           if(priceController)
                total += priceController.GetCurrentPrice();
        }

        return total;
    }

    [Button]
    public void UpdatePrice()
    {
        totalPrice.UpdateValue(GetTotalPrice());
    }

}
