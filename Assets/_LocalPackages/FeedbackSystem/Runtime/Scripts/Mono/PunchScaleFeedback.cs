using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Sirenix.OdinInspector;
public class PunchScaleFeedback : MonoBehaviour
{
    [SerializeField] Vector3 punchVector = new Vector3(0.3f, 0.3f, 0.3f);
    [SerializeField] float duration = 0.4f;
    [SerializeField] int vibrato = 10;
    [SerializeField] float elasticity = 10;

    [Button]
    public void Play()
    {
        transform.DOPunchScale(punchVector, duration, vibrato, elasticity);
    }
}
