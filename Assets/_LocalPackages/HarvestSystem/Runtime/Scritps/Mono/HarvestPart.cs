﻿using UnityEngine;
using UnityEngine.Events;

namespace HarvestSystem
{
    public class HarvestPart : MonoBehaviour
    {
        public UnityEvent OnHarvested;
        public bool IsHarvested { get; private set; }
        public bool IsCollected { get; private set; }
        public GameObject HarvestObject;
        public int DamageToHarvest;
        [Min(1)]public int Amount=1;
        Collider collectCollider;
        [SerializeField] string collecterTag;
        LayerMask initialLayer;

        private void Awake()
        {
            collectCollider = HarvestObject.GetComponent<Collider>();
            collectCollider.enabled = false;
            initialLayer = LayerMask.NameToLayer("Leaf");
            HarvestObject.layer = LayerMask.NameToLayer("IgnoreCollision");
        }

        public void Harvest()
        {
            if (IsHarvested) return;
            IsHarvested = true;
            PlayDropHarvestAnim();
            OnHarvested?.Invoke();
            collectCollider.enabled = true;
            Invoke(nameof(SetCollectableLayer), 1.5f);
        }

        void SetCollectableLayer()
        {
            //collectCollider.enabled = true;
            HarvestObject.layer = initialLayer;
        }

        void PlayDropHarvestAnim()
        {
            transform.parent = null;
            //yere dusme an'masyonu cagirilacak
        }

        private void OnCollisionEnter(Collision other) {
            GetComponent<Rigidbody>().velocity = Vector3.zero;
        }

        private void OnTriggerEnter(Collider other)
        {
            return;
            if (other.gameObject.CompareTag(collecterTag))
            {
                Collect();
            }
        }

        public void Collect()
        {
            IsCollected = true;
            Destroy(gameObject);
        }

    }
}
