using Sirenix.OdinInspector;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace AddonSystem
{
    public class AddonController : MonoBehaviour
    {
        [SerializeField] List<AddonObject> defaultAddons;
        [SerializeField] List<AddonEffect> addonEffects;
        [ReadOnly] public List<AddonObject> ActiveAddons;

        public static Action OnAddonUpdated;

        [Button]
        public void TryAddAddon(AddonObject addonObject)
        {
            for (int i = 0; i< addonObject.RequiredAddons.Length; i++)
            {
                if(ActiveAddons.Find(ao => ao == addonObject.RequiredAddons[i]).IsNull())
                {
                    GetAddonEffect(addonObject)?.OnAddonFailedToAdd?.Invoke();
                    return;
                }
            }

            var activeObject= ActiveAddons.Find(ao => ao == addonObject);
            var addonEffect = GetAddonEffect(addonObject);
            AddAddon(addonObject, addonEffect);
        }


        public void SetActiveAddons(List<AddonObject> activeAddons)
        {
            ActiveAddons.Clear();
            var addons = defaultAddons;
            addons.AddRange(activeAddons);
            addons = addons.Distinct().ToList();
            for (int i = 0; i < addons.Count; i++)
            {
               AddAddon(addons[i]);
            }
        }

        public void AddAddon(AddonObject addonObject)
        {
            var addonEffect = GetAddonEffect(addonObject);
            AddAddon(addonObject, addonEffect);
        }

        void AddAddon(AddonObject addonObject, AddonEffect addonEffect)
        {
            if (addonEffect.HasReachedTheCapacity)
            {
                addonEffect.OnAddonFailedToAdd?.Invoke();
                return;
            }
            if(addonEffect.CurrentAddonCount==0)
                ActiveAddons.Add(addonObject);
            addonEffect.CurrentAddonCount++;
            addonEffect?.OnAddonAdded?.Invoke();
            OnAddonUpdated?.Invoke();
            if (addonEffect.IsNull())
                addonObject.DefaultAddonAddedEffect(gameObject);
            else if (addonObject.ApplyDefaultAddedEffectAlways)
                addonObject.DefaultAddonAddedEffect(gameObject);
            //ActiveAddons=ActiveAddons.Distinct().ToList();
        }

        public AddonEffect GetAddonEffect(AddonObject addonObject)
        {
            return addonEffects.Find(af => af.AddonObject == addonObject);
        }

        public bool HasAddonHasReachedTheCapacity(AddonObject addonObject)
        {
            return GetAddonEffect(addonObject).HasReachedTheCapacity;
        }

        public void TryRemoveAddon(AddonObject addonObject)
        {
            var activeObject = ActiveAddons.Find(ao => ao == addonObject);
            if (activeObject!=null)
            {
                var addon = addonEffects.Find(af => af.AddonObject == addonObject);

                if (addon==null)
                    addonObject.DefaultAddonRemovedEffect(gameObject);
                else
                {
                    addon.CurrentAddonCount--;
                    if(addon.CurrentAddonCount<=0)
                        ActiveAddons.Remove(addonObject);
                    addon.OnAddonRemoved?.Invoke();
                    OnAddonUpdated?.Invoke();
                    if (addonObject.ApplyDefaultRemovedEffectAlways)
                        addonObject.DefaultAddonRemovedEffect(gameObject);
                }
            }
        }

        public void RemoveAllAddons()
        {
            for (int i = 0; i < 1000; i++)
            {
                if (ActiveAddons.Count > 0)
                {
                    TryRemoveAddon(ActiveAddons[0]);
                }
                else
                {
                    break;
                }
            }
        }

    }
}