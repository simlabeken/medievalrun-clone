using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace StackSystem
{
    // public class FixedStack : Stack
    // {
    //     [SerializeField] int stackWidth;
    //     [SerializeField] int stackHeight;
    //     [SerializeField] GameObject stackPrefab;

    //     [Button("Create Stack")]
    //     public override void CreateStack(List<StackElement> elementList)
    //     {
    //         CalculateStackPositions();

    //         StartCoroutine(IECreateStack(PositionList));
    //     }

    //     private void CalculateStackPositions()
    //     {
    //         PositionList.Clear();

    //         for (int i = 1; i <= stackHeight; ++i)
    //         {
    //             PositionList.Add(new List<Vector3>());
    //             for (int j = 1; j <= stackWidth; j = j + 2)
    //             {
    //                 if (stackWidth % 2 == 0)
    //                 {
    //                     Vector3 targetStackPosition = StackCenterTransform.position;
    //                     targetStackPosition.x += (StackData.Width * j * 1);
    //                     targetStackPosition.y += i * StackData.Height;
    //                     PositionList[i - 1].Add(targetStackPosition);

    //                     if (j + 1 > stackWidth) continue;
    //                     targetStackPosition = StackCenterTransform.position;
    //                     targetStackPosition.x += (StackData.Width * j * -1);
    //                     targetStackPosition.y += i * StackData.Height;
    //                     PositionList[i - 1].Add(targetStackPosition);
    //                 }
    //                 else
    //                 {
    //                     Vector3 targetStackPosition = StackCenterTransform.position;
    //                     targetStackPosition.x += (StackData.Width / 2 * j * 1);
    //                     targetStackPosition.y += i * StackData.Height;
    //                     PositionList[i - 1].Add(targetStackPosition);

    //                     if (j + 1 > stackWidth) continue;
    //                     targetStackPosition = StackCenterTransform.position;
    //                     targetStackPosition.x += (StackData.Width / 2 * j * -1);
    //                     targetStackPosition.y += i * StackData.Height;
    //                     PositionList[i - 1].Add(targetStackPosition);
    //                 }
    //             }
    //         }
    //     }

    //     void MoveElements()
    //     {

    //     }

    //     IEnumerator IECreateStack(List<List<Vector3>> positionList)
    //     {
    //         for (int i = 0; i < positionList.Count; i++)
    //         {
    //             for (int j = 0; j < positionList[i].Count; j++)
    //             {
    //                 yield return new WaitForSeconds(StackData.StackDelay);
    //                 Instantiate(stackPrefab, positionList[i][j], Quaternion.identity);
    //                 if((stackWidth % 2 == 1 && j % 2 == 0)) continue;
    //                 if(++j > positionList[i].Count - 1) continue;
    //                 Instantiate(stackPrefab, positionList[i][j], Quaternion.identity);
    //             }
    //         }
    //     }
    // }
}