using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;

namespace StackSystem
{
    public abstract class Stack : MonoBehaviour, ICanStack
    {
        [SerializeField] protected StackData StackData;
        [SerializeField] protected Transform StackCenterTransform;
        [SerializeField] protected bool LocalMovement = true;
        [ReadOnly] [SerializeField] protected int StackWidth;
        [ReadOnly] [SerializeField] protected int StackHeight;
        protected float TotalStackDelay { get { return StackData.MoveCenter.Enabled ? StackData.MoveCenter.Value + StackData.StackDelay + StackData.MoveDelay : StackData.StackDelay + StackData.MoveDelay; } }

        public UnityEvent<IStackable> OnStack => throw new System.NotImplementedException();

        public UnityEvent<IStackable> OnUnStack => throw new System.NotImplementedException();

        protected List<List<Vector3>> PositionList = new List<List<Vector3>>();
        protected List<StackElement> ElementList = new List<StackElement>();
        /// <summary>Initiates the creation of the stack with the given size. A prefab must be provided.</summary>
        public virtual void CreateStack(int elementSize) {}
        /// <summary>Initiates the creation of the stack with the given element list.</summary>
        public virtual void CreateStack(List<StackElement> elementList) {}
        /// <summary>Creates stack elements</summary>
        protected abstract IEnumerator IECreateStack();
        /// <summary>Calculates the position of the elements in the stack with respect to the stack data parameters.</summary>
        protected abstract void CalculateElementPositions();
        /// <summary>Removes the given stack element from the stack if it exists.</summary>
        public virtual StackElement PopFromStack() { throw new System.NotImplementedException(); }
        /// <summary>Aligns the position of the stack elements.</summary>
        protected virtual void AlignStack() { throw new System.NotImplementedException(); }
        /// <summary>Moves the given element to the target position</summary>
        protected virtual void MoveElementToPosition(StackElement element, Vector3 position)
        {
            if(!LocalMovement)
                element.transform.DOMove(position, StackData.MoveDelay);
            else
                element.transform.DOLocalMove(position, StackData.MoveDelay);
        }
        /// <summary>Starts the movement of a single element into the stack</summary>
        protected virtual void StartStackMovement(StackElement element, Vector3 stackPosition)
        {
            element.transform.parent = StackCenterTransform;
            StartCoroutine(IEStartStackMovement(element, stackPosition));
        }

        protected void RemoveOldStack(StackElement element)
        {
            if (!ElementList.Contains(element))
            {
                element.OnRemoved?.Invoke(element);
                element.OnRemoved += OnStackElementRemoved;
            }
        }

        protected virtual IEnumerator IEStartStackMovement(StackElement element, Vector3 stackPosition)
        {
            if(StackData.MoveCenter.Enabled)
            {
                Vector3 centerOffset = new Vector3(0, 0, 0);
                if(!LocalMovement)
                    MoveElementToPosition(element, StackCenterTransform.position + centerOffset);
                else
                    MoveElementToPosition(element, Vector3.zero + centerOffset);
                yield return new WaitForSeconds(StackData.MoveCenter.Value);
            }
            MoveElementToPosition(element, stackPosition);
        }
        protected virtual void OnStackElementRemoved(StackElement stackElement) 
        { 
            if(ElementList.Contains(stackElement))
                ElementList.Remove(stackElement);
        }
        protected virtual void OnEnable() {}
        protected virtual void OnDisable() {}

        void ICanStack.Stack(IStackable IStackable)
        {
        }

        public void UnStack(IStackable IStackable)
        {
            throw new System.NotImplementedException();
        }

        void ICanStack.StackList(List<IStackable> IStackables)
        {
            // List<StackElement> lp = IStackables.ConvertAll(new Converter<IStackable, StackElement>(IStackables));

            List<StackElement> stackElementList = new List<StackElement>();
            foreach(var stackable in IStackables)
            {
                stackElementList.Add(((Component)stackable).GetComponent<StackElement>());
            }
            // IStackables.Cast<StackElement>().ToList();
            CreateStack(stackElementList);
        }

        public void SetStackTarget(Transform target)
        {
            StackCenterTransform = target;
        }
    }
}