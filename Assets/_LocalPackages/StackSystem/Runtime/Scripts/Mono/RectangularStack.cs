using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;

namespace StackSystem
{
    public class RectangularStack : Stack
    {
        public static Action OnStackCompleted;
        [Toggle("Enabled")]
        public IntegerToggleable FixedWidth = new IntegerToggleable();
        [Toggle("Enabled")]
        public IntegerToggleable FixedHeight = new IntegerToggleable();
        [Toggle("Enabled")]
        public GameObjectToggleable UsePrefab = new GameObjectToggleable();

        [SerializeField] List<StackElement> testStackList;
        // [Button("Set Stack Elements")]
        // void SetStackElementsTest()
        // {
        //     //ElementList = testStackList;
        //     CreateStack(testStackList);
        // }

        [Button("Create Stack")]
        public override void CreateStack(int elementSize)
        {
            for (int i = 0; i < elementSize; i++)
            {
                ElementList.Add(new StackElement());
            }
            CalculateStackSize(FixedWidth.Enabled, FixedHeight.Enabled, elementSize);
            CalculateElementPositions();
            StartCoroutine(IECreateStack());
        }

        [Button("Create Stack with List")]
        public override void CreateStack(List<StackElement> elementList)
        {
            ElementList = elementList;
            CalculateStackSize(FixedWidth.Enabled, FixedHeight.Enabled, elementList.Count);
            CalculateElementPositions();
            StartCoroutine(IECreateStack());
        }

        protected override void CalculateElementPositions()
        {
            PositionList.Clear();

            for (int i = 1; i <= StackHeight; ++i)
            {
                PositionList.Add(new List<Vector3>());
                for (int j = 1; j <= StackWidth; j = j + 2)
                {
                    if (StackWidth % 2 == 0)
                    {
                        Vector3 targetStackPosition = Vector3.zero;
                        if(!LocalMovement)
                            targetStackPosition = StackCenterTransform.position;
                        targetStackPosition.x += (StackData.Width * j * 1);
                        targetStackPosition.y += i * StackData.Height;
                        PositionList[i - 1].Add(targetStackPosition);

                        if (j + 1 > StackWidth) continue;
                        targetStackPosition = Vector3.zero;
                        if(!LocalMovement)
                            targetStackPosition = StackCenterTransform.position;
                        targetStackPosition.x += (StackData.Width * j * -1);
                        targetStackPosition.y += i * StackData.Height;
                        PositionList[i - 1].Add(targetStackPosition);
                    }
                    else
                    {
                        Vector3 targetStackPosition = Vector3.zero;
                        if(!LocalMovement)
                            targetStackPosition = StackCenterTransform.position;
                        targetStackPosition.x += (StackData.Width / 2 * j * 1);
                        targetStackPosition.y += i * StackData.Height;
                        PositionList[i - 1].Add(targetStackPosition);

                        if (j + 1 > StackWidth) continue;
                        targetStackPosition = Vector3.zero;
                        if(!LocalMovement)
                            targetStackPosition = StackCenterTransform.position;
                        targetStackPosition.x += (StackData.Width / 2 * j * -1);
                        targetStackPosition.y += i * StackData.Height;
                        PositionList[i - 1].Add(targetStackPosition);
                    }
                }
            }
        }

        protected void CalculateStackSize(bool fixedWidth, bool fixedHeight, int elementSize)
        {
            StackWidth = 0;
            StackHeight = 0;

            if(fixedWidth && !fixedHeight)
            {
                StackWidth = FixedWidth.Value;
                StackHeight = elementSize / FixedWidth.Value;
            }
            else if(!fixedWidth && fixedHeight)
            {
                StackHeight = FixedHeight.Value;
                StackWidth = elementSize / FixedHeight.Value;
            }
            else
            {
                Debug.LogError("Dynamic height and width is not allowed");
            }

            ++StackHeight;
        }

        protected override IEnumerator IECreateStack()
        {
            for (int i = 0, k = 0; i < PositionList.Count; ++i)
            {
                for (int j = 0; j < PositionList[i].Count && k < ElementList.Count; ++j, ++k)
                {
                    yield return new WaitForSeconds(TotalStackDelay);
                    if(UsePrefab.Enabled)
                        Instantiate(UsePrefab.Value, PositionList[i][j], Quaternion.identity);
                    else
                        StartStackMovement(ElementList[k], PositionList[i][j]);
                    if((StackWidth % 2 == 1 && j % 2 == 0)) continue;
                    if(++j > PositionList[i].Count - 1) continue;
                    if(++k > ElementList.Count - 1) continue;

                    if(UsePrefab.Enabled)
                        Instantiate(UsePrefab.Value, PositionList[i][j], Quaternion.identity);
                    else
                        StartStackMovement(ElementList[k], PositionList[i][j]);
                    //Debug.Log("k: " + k + " element list count: " + ElementList.Count);
                }
            }

            OnStackCompleted?.Invoke();
        }
    }
    
}