using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

[CreateAssetMenu(fileName = "StackData", menuName = "StackSystem/Create Stack Data")]
public class StackData : ScriptableObject 
{
    public float Width;
    public float Height;
    public float MoveDelay;
    [Toggle("Enabled")]
    public FloatToggleable MoveCenter;
    public float StackDelay;
}
