﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace ProgressSystem
{
    [CreateAssetMenu(menuName = "ProgressSystem/ProgressType")]
    public class ProgressType : ScriptableObject
    {
        [SerializeField] int startExp = 0;
        [ReadOnly] public int CurrentExp = 0;
        [ReadOnly] [SerializeField] int totalCurrentExp = 0;
        public int TotalCurrentExp
        {
            get
            {
                totalCurrentExp = 0;
                for (int i = 0; i < CurrentLevel-1; i++)
                {
                    totalCurrentExp += MaxExps[i];
                }
                totalCurrentExp+=CurrentExp;
                return totalCurrentExp;
            }
        }

        [SerializeField] [Min(1)] int startLevel = 1;
        [ReadOnly] [Min(1)] public int CurrentLevel;
        public int[] MaxExps;
        int currentMaxExp { get { return MaxExps[Mathf.Clamp(CurrentLevel-1,0, MaxExps.Length-1)]; } }
        [ReadOnly] [SerializeField] int totalMaxExp = 0;
        public int TotalMaxExp
        {
            get
            {
                totalMaxExp = 0;
                for (int i = 0; i < MaxExps.Length; i++)
                {
                    totalMaxExp += MaxExps[i];
                }
                return totalMaxExp;
            }
        }


        public float CurrentProgressRatio { get { return 1f * CurrentExp / currentMaxExp; } }
        public float TotalProgressRatio { get { return 1f * TotalCurrentExp / TotalMaxExp; } }

        public UnityEvent<int> OnLevelUpdated;
        public UnityEvent OnLevelUp;
        public UnityEvent OnLevelDown;
        public UnityEvent OnProgressBelowZero;
        public UnityEvent OnProgressReachedMax;
        public UnityEvent<int> OnXpIncreased;
        public UnityEvent<int> OnXpDecreased;
        public UnityEvent<int> OnCurrentXpSet;
        public UnityEvent<float> OnProgressRatioChanged;
        public UnityEvent<float> OnTotalProgressRatioChanged;

        public void AddExp(int value)
        {
            CurrentExp += value;
            UpdateProgress();
            if (value != 0)
                OnXpIncreased?.Invoke(value);
            OnProgressRatioChanged?.Invoke(CurrentProgressRatio);
            OnTotalProgressRatioChanged?.Invoke(TotalProgressRatio);
        }

        public void RemoveExp(int value)
        {
            CurrentExp -= value;
            UpdateProgress();
            if (value != 0)
                OnXpDecreased?.Invoke(value);
            OnProgressRatioChanged?.Invoke(CurrentProgressRatio);
            OnTotalProgressRatioChanged?.Invoke(TotalProgressRatio);
        }

        void UpdateProgress()
        {
            CalculateExp();
            OnCurrentXpSet?.Invoke(CurrentExp);
            OnLevelUpdated?.Invoke(CurrentLevel);
        }

        void CalculateExp()
        {
            if (CurrentExp >= currentMaxExp)
            {
                if (CurrentLevel < MaxExps.Length)
                {
                    CurrentExp -= currentMaxExp;
                    LevelUp();
                    CalculateExp();
                }
                else
                {
                    CurrentExp = currentMaxExp;
                    OnProgressReachedMax?.Invoke();
                }
            }
            else if (CurrentExp < 0)
            {
                if (CurrentLevel > 0)
                {
                    LevelDown();
                    CurrentExp += currentMaxExp;
                    CalculateExp();
                }
                if(CurrentLevel <= 0)
                {
                    CurrentExp = 0;
                    CurrentLevel = 0;
                    OnProgressBelowZero?.Invoke();
                }
            }
        }

        public void Reset()
        {
            SetLevel(startLevel);
            SetXP(startExp);
        }


        [Button("Level Up")]
        void LevelUp()
        {
            if (MaxExps.Length <= CurrentLevel) return;
                SetLevel(CurrentLevel + 1);
            OnLevelUp?.Invoke();
        }

        public static void LevelUp(ProgressType progressType)
        {
            progressType.LevelUp();
        }


        [Button("Level Down")]
        void LevelDown()
        {
            if (CurrentLevel<=0) return;
            SetLevel(CurrentLevel - 1);
            OnLevelDown?.Invoke();
        }

        public static void LevelDown(ProgressType progressType)
        {
            progressType.LevelDown();
        }


        void SetLevel(int level)
        {
            CurrentLevel = level;
            OnLevelUpdated?.Invoke(level);
        }

        void SetXP(int Exp)
        {
            CurrentExp = Exp;
            OnCurrentXpSet?.Invoke(Exp);
            OnProgressRatioChanged?.Invoke(CurrentProgressRatio);
            OnTotalProgressRatioChanged?.Invoke(TotalProgressRatio);
        }

    }
}