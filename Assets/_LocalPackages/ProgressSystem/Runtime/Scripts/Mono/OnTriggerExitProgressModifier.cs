﻿using UnityEngine;

namespace ProgressSystem
{
    public class OnTriggerExitProgressModifier : OnTriggerProgressModifier
    {
        private void OnTriggerExit(Collider other)
        {
            OnTrigger(other);
        }
    }

}