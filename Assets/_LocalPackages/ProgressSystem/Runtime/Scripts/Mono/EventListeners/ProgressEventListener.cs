﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace ProgressSystem
{
    
    public abstract class ProgressEventListener : MonoBehaviour
    {
        [SerializeField] protected ProgressType progressType;
        [ReadOnly]   [SerializeField]  ProgressType currentProgressType;

        [ReadOnly][SerializeField] int currentLevel;

        void OnValidate()
        {
            SetCurrentLevel(progressType.CurrentLevel);
        }

        public void SetProgressType(ProgressType progressType)
        {
            if (!currentProgressType.IsNull())
                RemoveListeners(currentProgressType);
            currentProgressType = progressType;
            //AddListeners(ref currentProgressType);
            AddListeners(currentProgressType);
        }
        [Button("Add Listeners")]
        void AddListenerTest()
        {
            AddListeners(progressType);
            // AddListeners(ref progressType);
        }
        private void AddListeners(ProgressType pt)
        {
            if (pt.IsNull()) return;
            pt.OnLevelUpdated.AddListener(OnLevelUpdated);
            pt.OnLevelUpdated.AddListener(SetCurrentLevel);
            pt.OnLevelDown.AddListener(OnLevelDown);
            pt.OnLevelUp.AddListener(OnLevelUp);
            pt.OnProgressRatioChanged.AddListener(OnProgressRatioChanged);
            pt.OnTotalProgressRatioChanged.AddListener(OnTotalProgressRatioChanged);
        }

        public void RemoveListeners(ProgressType pt)
        {
            if (pt.IsNull()) return;
            pt.OnLevelUpdated.RemoveListener(OnLevelUpdated);
            pt.OnLevelUpdated.RemoveListener(SetCurrentLevel);
            pt.OnLevelDown.RemoveListener(OnLevelDown);
            pt.OnLevelUp.RemoveListener(OnLevelUp);
            pt.OnProgressRatioChanged.RemoveListener(OnProgressRatioChanged);
            pt.OnTotalProgressRatioChanged.RemoveListener(OnTotalProgressRatioChanged);
        }

        protected void SetCurrentLevel(int level)
        {
            currentLevel = level;
        }

        void OnEnable()
        {
            if (currentProgressType.IsNull())
                SetProgressType(progressType);
        }

        void OnDisable()
        {
            if (!currentProgressType.IsNull())
            {
                RemoveListeners(currentProgressType);
                currentProgressType = null;
            }
        }

        [Button("On Level Updated")]
        protected virtual void OnLevelUpdated(int level) { }
        [Button("On Level Down")]
        protected virtual void OnLevelDown() { }
        [Button("On Level Up")]
        protected virtual void OnLevelUp() { }
        [Button("On Progress Ratio Changed")]
        protected virtual void OnProgressRatioChanged(float ratio) { }
        protected virtual void OnTotalProgressRatioChanged(float ratio) { }
    }
}