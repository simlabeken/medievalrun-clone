﻿namespace ProgressSystem
{
    public class CurrentProgressImage : ProgressImage
    {

        protected override void OnProgressRatioChanged(float ratio)
        {
            SetFillAmount(ratio);
        }
    }
}