﻿using UnityEngine;
using UnityEngine.UI;

namespace ProgressSystem
{
    public class CurrentProgressBar : ProgressBar
    {
        private void Start()
        {
            if(setOnStart)
            SetBar(progressType.CurrentProgressRatio);
        }

        protected override void OnProgressRatioChanged(float ratio)
        {
            SetBar(ratio);
        }
    }
}