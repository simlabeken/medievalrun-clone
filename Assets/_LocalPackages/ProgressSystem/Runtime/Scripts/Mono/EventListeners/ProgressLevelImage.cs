﻿using UnityEngine;
using UnityEngine.UI;

namespace ProgressSystem
{
    public class ProgressLevelImage : ProgressEventListener
    {
        [SerializeField] Image progressImage;
        [SerializeField] Sprite[] levelSprites;

        protected override void OnLevelUpdated(int level)
        {
            SetImage(level);
        }

        public void SetImage(int level)
        {
            if (progressImage == null) return;
            if (levelSprites.Length == 0) return;
            int index = Mathf.Clamp(level - 1, 0, levelSprites.Length - 1);
            progressImage.sprite = levelSprites[index];
        }

    }
}