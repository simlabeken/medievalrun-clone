﻿using UnityEngine;
using UnityEngine.Events;

namespace ProgressSystem
{

    public abstract class OnTriggerProgressModifier : MonoBehaviour, ICanModifyProgress
    {
        [SerializeField] UnityEvent onModifyProgress;
        public UnityEvent OnModifyProgress => onModifyProgress;

        [SerializeField] int modifyValue;
        public int ModifyValue => modifyValue;

        [SerializeField] ProgressType progressType;
        public ProgressType ProgressType => progressType;

        protected virtual void OnTrigger(Collider other)
        {
            var progressable = other.GetComponent<IProgressable>();
            if(IsModifyable(progressable))
                Modify(progressable);

        }

        protected bool IsModifyable(IProgressable progressable)
        {
            if (!progressable.IsNull())
            {
                if (ProgressType.IsNull() || ProgressType == progressable.ProgressType)
                    return true;
            }
            return false;

        }

        public void Modify(IProgressable progressable)
        {
            progressable.Modify(modifyValue);
            OnModifyProgress?.Invoke();
        }
    }

}