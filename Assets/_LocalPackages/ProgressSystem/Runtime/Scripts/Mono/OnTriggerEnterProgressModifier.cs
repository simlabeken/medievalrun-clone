﻿using UnityEngine;

namespace ProgressSystem
{
    public class OnTriggerEnterProgressModifier : OnTriggerProgressModifier
    {
        private void OnTriggerEnter(Collider other)
        {
            OnTrigger(other);
        }
    }

}