﻿using UnityEngine;
using UnityEngine.Events;

namespace ProgressSystem
{

    public interface IProgressable
    {
        ProgressType ProgressType { get; }
        UnityEvent<int> OnProgressModified { get; }

        void Modify(int value);

    }
}