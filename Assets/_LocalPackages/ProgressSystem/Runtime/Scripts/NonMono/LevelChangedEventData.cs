﻿using System;
using UnityEngine.Events;

namespace ProgressSystem
{

    [Serializable]
    public class LevelChangedEventData
    {
        [ReadOnly]public int Index;
        public UnityEvent LevelDownEvent;
        public UnityEvent LevelUpEvent;
    }
}