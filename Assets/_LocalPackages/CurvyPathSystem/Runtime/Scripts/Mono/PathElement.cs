using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FluffyUnderware.Curvy;
using Sirenix.OdinInspector;
using FluffyUnderware.Curvy.Controllers;

namespace CurvyPathSystem
{
    [RequireComponent(typeof(SplineController))]
    public class PathElement : MonoBehaviour
    {
        [SerializeField] CurvySpline spline;
        [SerializeField] GameObject model;
        [SerializeField] Vector2 positionOffsetVector;
        [SerializeField] Vector3 rotationOffsetVector;
        [Tooltip("Enables the usage of scriptable x offset data. X value of position offset vector will be disabled.")]
        [Toggle("Enabled")]
        public ScriptableFloatToggleable xOffsetData;
        [Tooltip("Enables the usage of scriptable y offset data. Y value of position offset vector will be disabled.")]
        [Toggle("Enabled")]
        public ScriptableFloatToggleable yOffsetData;
        bool autoSetPositionOffsetOnLock = true;
        [SerializeField] [ReadOnly] bool locked;
        Vector3? elementPosition = Vector3.zero;
        Vector3 curveCenter = Vector3.zero;
        SplineController _splineController;
        SplineController splineController
        {
            get
            {
                if(_splineController == null)
                {
                    _splineController = GetComponent<SplineController>();
                    if(_splineController)
                        _splineController.PositionMode = CurvyPositionMode.WorldUnits;
                }
                if(_splineController == null)
                {
                    _splineController = gameObject.AddComponent<SplineController>();
                    _splineController.PositionMode = CurvyPositionMode.WorldUnits;
                }
                return _splineController;
            }
        }

        [Button("Lock")]
        void LockSplineController()
        {
            autoSetPositionOffsetOnLock = true;
            splineController.Spline = spline;
            CalculatePosition();
            autoSetPositionOffsetOnLock = false;
            locked = true;
        }

        [Button("Unlock")]
        void UnlockSplineController()
        {
            // autoSetPositionOffsetOnLock = true;
            splineController.Spline = null;
            transform.position = transform.TransformPoint(model.transform.localPosition);
            model.transform.localPosition = Vector3.zero;
            locked = false;
            // if(autoSetPositionOffsetOnLock)
            // {
            //     // transform.position += model.transform.localPosition;
            //     // Debug.Log("Transform point: " + transform.TransformPoint(model.transform.position));
            // }
        }

        void CalculatePosition()
        {
            if(locked) return;

            var mTF = spline.GetNearestPointTF(transform.localPosition, out curveCenter);
            splineController.RelativePosition = mTF;
            if(autoSetPositionOffsetOnLock)
            {
                positionOffsetVector.x = 0;
                // model.transform.localPosition = positionOffsetVector;
                Vector3 oldPosition = transform.position;// + new Vector3(positionOffsetVector.x, positionOffsetVector.y, 0);
                var xOffsetDistance = Vector3.Distance(transform.position, curveCenter);
                // model.transform.localPosition = positionOffsetVector;
                // model.transform.localRotation = Quaternion.Euler(rotationOffsetVector);

                var direction = (oldPosition - curveCenter).normalized;
                var inverseDirection = transform.InverseTransformDirection(direction);
                // Debug.Log("Current position: " + curveCenter);
                // Debug.Log("Old position: " + oldPosition);
                // Debug.Log("Inverse: " + (inverseDirection));
                Debug.Log("Distance: " + xOffsetDistance);
                if(inverseDirection.x > 0)
                    positionOffsetVector.x = xOffsetDistance;
                else if(inverseDirection.x < 0)
                    positionOffsetVector.x = -xOffsetDistance;
                else
                    Debug.LogError("Direction is 0");

                // positionOffsetVector.x = xOffsetDistance;

            }
            UpdatePosition();
            // Vector3 targetPos = transform.position + new Vector3(positionOffsetVector.x, positionOffsetVector.y, 0);
            // var inversePos = model.transform.InverseTransformPoint(targetPos);
            // inversePos.z = 0;
            // model.transform.localPosition = inversePos;
            // Debug.Log("Transform Inverse Pos: " + model.transform.InverseTransformPoint(targetPos));
            // model.transform.localPosition = positionOffsetVector;
        }

        void OnValidate()
        {
            if(model == null)
                model = transform.Find("Model").gameObject;
            if(model == null)
                Debug.LogError("Model not found on the object", gameObject);

            // if(!autoSetPositionOffsetOnLock && locked)
            // {
            //     Debug.Log("Local position before: " + model.transform.localPosition);
            //     Vector3 targetPos = transform.position + new Vector3(positionOffsetVector.x, positionOffsetVector.y, 0);
            //     var inversePos = model.transform.InverseTransformPoint(targetPos);
            //     inversePos.z = 0;
            //     model.transform.localPosition = inversePos;
            //     Debug.Log("Local position after: " + model.transform.localPosition);
            //     Debug.Log("Offset vector: " + positionOffsetVector);
            //     Debug.Log("Inverse pos: " + inversePos);

            //     // model.transform.localPosition = positionOffsetVector;
            // }
                // model.transform.localPosition = model.transform.InverseTransformPoint(positionOffsetVector);
            if(locked)
                UpdatePosition();
        }

        // [Button("Update Position")]
        void UpdatePosition()
        {
            Vector3 targetPos = transform.position + new Vector3(xOffsetData.Enabled ? xOffsetData.Value.GetValue() : positionOffsetVector.x, 
            yOffsetData.Enabled ? yOffsetData.Value.GetValue() : positionOffsetVector.y, transform.position.z);
            // var inversePos = transform.InverseTransformPoint(targetPos);
            var inversePos = transform.InverseTransformPointUnscaled(targetPos);
            inversePos.z = 0;

            Debug.Log("Transform position: " + transform.position);
            Debug.Log("Offset vector: " + positionOffsetVector);
            Debug.Log("Inverse pos: " + inversePos);
            // Debug.Log("Transform inverse pos: " + inversePos);
            model.transform.localPosition = inversePos;
            // model.transform.localPosition = new Vector3(xOffsetData.Enabled ? xOffsetData.Value.Value : inversePos.x,
            // yOffsetData.Enabled ? yOffsetData.Value.Value : inversePos.y,
            // zOffsetData.Enabled ? zOffsetData.Value.Value : inversePos.z);
            model.transform.localRotation = Quaternion.Euler(rotationOffsetVector);

        }

        void OnDrawGizmos()
        {
            if(locked == false)
            {
                Gizmos.DrawIcon(model.transform.position, Path.Combine("Hell Tap Entertainment", "PoolKit", "PoolKitSpawnPoint"), false, Color.red);
            }
        }
    }
}