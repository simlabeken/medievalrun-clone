﻿using System;
using UnityEngine;

namespace GroupMovement.ChainMovementSystem
{
    [Serializable]
    public class TransformNodeAlignerWorld : NodeAligner
    {
        public TransformNodeAlignerWorld(ChainNode node) : base(node)
        {
          
        }

        public override void AlignWithX(Transform target, float alignmentSpeed)
        {
        }

        public override void AlignWithY(Transform target, float alignmentSpeed)
        {
        }

        public override void AlignWithZ(Transform target, float alignmentSpeed)
        {
        }
    }
}
