﻿using System.Collections;
using UnityEngine;

namespace GroupMovement.ChainMovementSystem
{
    public class NodeThrower : NodeRemover
    {
        
        [SerializeField] protected float throwForce = 10;

        protected override void RemoveNode(ChainNode node)
        {
            base.RemoveNode(node);
            ThrowNode(node);
        }

        protected virtual void ThrowNode(ChainNode node)
        {
            StartCoroutine(IEThrowNodeWithDelay(node,  Time.fixedDeltaTime));  
        }
        protected virtual Vector3 GetDirection(ChainNode node)
        {
           return (node.transform.position - transform.position).normalized;
        }
        IEnumerator IEThrowNodeWithDelay(ChainNode node, float delay)
        {
                var direction = GetDirection(node);
                Debug.Log(direction);
                node.GetComponent<Rigidbody>().isKinematic = true;
            //Debug.Break();
            yield return new WaitForSecondsRealtime(delay);
            for (int i = 0; i < 100; i++)
            {
                yield return new WaitForSecondsRealtime(Time.fixedDeltaTime);
                node.transform.position+= direction * throwForce* Time.fixedDeltaTime;
            }
        }
    }
}
