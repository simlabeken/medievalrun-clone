﻿namespace CurrencySystem
{
    public enum CurrencyCondition
    {
        Equal,
        Greater,
        Less
    }
}