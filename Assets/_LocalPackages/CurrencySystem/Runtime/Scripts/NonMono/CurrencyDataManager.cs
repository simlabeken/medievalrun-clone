﻿using System;
using System.Threading.Tasks;

namespace CurrencySystem
{
    [Serializable]
    public abstract class CurrencyDataManager
    {
        protected CurrencyType currencyType;

        public float CurrencyAmount { get { return currencyType.Amount; } protected set { currencyType.Amount = value; } }

        public CurrencyDataManager(CurrencyType type)
        {
            currencyType = type;
            //if (!HasDefaultData())
            //    CreateDefaultSaveData();
        }

        public abstract void SaveCurrency(CurrencyData data);       
        public virtual Task LoadCurrencyDataAsync()
        {
            Task.Delay(1);
            return Task.CompletedTask;
        }

        public abstract bool HasDefaultData();

        protected void CreateDefaultSaveData()
        {
            var data = new CurrencyData();
           // data.CurrencyType = currencyType;
            SaveCurrency(data);
        }


}

}