﻿using UnityEngine;

namespace CurrencySystem
{
    public class Exchanger : MonoBehaviour
    {
        [SerializeField] CurrencyType firstCurrencyType;
        [SerializeField] CurrencyType secondCurrencyType;
        [SerializeField] [Min(.01f)] float firstToSecondExchangeRate=1;
        Currency firstCurrency;
        Currency secondCurrency;

        private void Start()
        {
            firstCurrency = new Currency(firstCurrencyType);
            secondCurrency = new Currency(secondCurrencyType);
        }


        public void Exchange(CurrencyType type, float value)
        {
            if (type == firstCurrencyType)
                ExchangeFirstToSecond(value);
            else if (type == secondCurrencyType)
                ExchangeSecondToFirst(value);
            else
                Debug.Log("This exchanger can't exchange this currency");
        }

        public void ExchangeFirstToSecond(float value)
        {
            if(firstCurrency.GetAmount()<value)
            {
                Debug.Log("Not enoung currency to exchange");
                return;
            }
            firstCurrency.IncreaseAmount(-value);
            secondCurrency.IncreaseAmount(value* firstToSecondExchangeRate);
        }

        public void ExchangeSecondToFirst(float value)
        {
            if (secondCurrency.GetAmount() < value)
            {
                Debug.Log("Not enoung currency to exchange");
                return;
            }
            secondCurrency.IncreaseAmount(-value );
            firstCurrency.IncreaseAmount(value / firstToSecondExchangeRate);
        }
    }
}