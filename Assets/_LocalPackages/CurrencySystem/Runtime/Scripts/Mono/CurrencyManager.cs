using System.Collections;
using System.Collections.Generic;
using CurrencySystem;
using UnityEngine;


namespace CurrencySystem
{
    public class CurrencyManager : MonoBehaviour
{
    [SerializeField] CurrencyType currencyType;
    [ReadOnly] [SerializeField] Currency currency;
    public static CurrencyManager Instance;

    private void Awake()
    {
        Instance = this;
        currency = new Currency(currencyType);
        currencyType.LoadCurrency();
    }

    public void AddCurrency(int value)
    {
        currency.IncreaseAmount(value);
    }

    public void RemoveCurrency(int value)
    {
        currency.RemoveAmount(value);
    }

    public float GetCurrentAmount()
    {
        return currency.GetAmount();
    }

    public void ResetCurrency()
    {
        currency.SetCurrencyAmount(0);
    }
}
}
