﻿
using UnityEngine;

public static partial class Extension
{
    public static void Min(this int value, int minValue)
    {
        value = Mathf.Max(value, minValue);
    }

    public static void Max(this int value, int maxValue)
    {
        value = Mathf.Min(value, maxValue);
    }

    public static void Clamp(this int value, int minValue, int maxValue)
    {
        value = Mathf.Clamp(value, minValue, maxValue);
    }
}