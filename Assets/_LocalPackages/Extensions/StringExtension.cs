﻿
public static partial class Extension
{
    public static bool CompareWithoutCaseSensitivity(this string value, string compareString)
    {
        return value.ToLower() == compareString.ToLower();
    }

    public static char GetLastChar(this string value)
    {
        return string.IsNullOrEmpty(value)?' ': value[value.Length-1];
    }
}