﻿
using UnityEngine;

public static partial class Extension
{
    public static void Min(this float value, float minValue)
    {
        value = Mathf.Max(value, minValue);
    }

    public static void Max(this float value, float maxValue)
    {
        value = Mathf.Min(value, maxValue);
    }

    public static void Clamp(this float value, float minValue, float maxValue)
    {
        value = Mathf.Clamp(value, minValue, maxValue);
    }

}