﻿using ScriptableSystem;
using UnityEngine;
using UnityEngine.Events;

namespace DamagableSystem
{
    public class Damagable : MonoBehaviour, IDamagable
    {
        public FloatReference Health;
        public UnityEvent OnDamaged;
        public UnityEvent OnHealthBelowZero;

        public void ApplyDamage(float damage)
        {
            Health.Value -= damage;
            OnDamaged?.Invoke();
            if (Health.Value <= 0)
                OnHealthBelowZero?.Invoke();
        }
    }
}