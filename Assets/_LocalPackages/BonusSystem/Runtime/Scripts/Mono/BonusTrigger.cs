using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BonusSystem
{
    public class BonusTrigger : MonoBehaviour
    {
        public Action OnTriggerReached;
        [SerializeField] string compareTag;

        private void OnTriggerEnter(Collider other) 
        {
            if(other.CompareTag(compareTag))
                OnTriggerReached?.Invoke();
        }
    }
}