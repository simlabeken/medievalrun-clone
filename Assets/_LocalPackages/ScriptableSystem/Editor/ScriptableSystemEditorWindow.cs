using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class ScriptableSystemEditorWindow : EditorWindow
{


    // Add menu named "My Window" to the Window menu
    [MenuItem("Window/ScriptableSystemEditorWindow")]
    public static void Init()
    {
        // Get existing open window or if none, make a new one:
        ScriptableSystemEditorWindow window = (ScriptableSystemEditorWindow)EditorWindow.GetWindow(typeof(ScriptableSystemEditorWindow));
        window.Show();
    }

    void OnGUI()
    {
        //GUILayout.Label("Base Settings", EditorStyles.boldLabel);
        //myString = EditorGUILayout.TextField("Text Field", myString);
        //groupEnabled = EditorGUILayout.BeginToggleGroup("Optional Settings", groupEnabled);
        //myBool = EditorGUILayout.Toggle("Toggle", myBool);
        //myFloat = EditorGUILayout.Slider("Slider", myFloat, -3, 3);
        //EditorGUILayout.EndToggleGroup();
    }

    //List<ScriptableObject> TypesExample(string type)
    //{
    //    Debug.Log("*** FINDING ASSETS BY TYPE ***");
    //    string[] guids;

    //    guids = AssetDatabase.FindAssets("t:" + type);
    //    var t = Type.GetType(type)
    //    var objects = new List<>();
    //    foreach (string guid in guids)
    //    {
    //        var path = AssetDatabase.GUIDToAssetPath(guid);
    //        Debug.Log("ScriptObj: " + path);
    //        objects.Add(AssetDatabase.LoadAssetAtPath< Type.GetType(type)> (path));
    //    }
    //    return objects;
    //}

}
