﻿using UnityEngine;

namespace ScriptableSystem.ScriptableBehaviour
{
    [CreateAssetMenu(menuName = "ScriptableSystem/ScriptableBehaviour/ScriptableRigidbodyKinematicToggler")]
    public class ScriptableRigidbodyKinematicToggler : ScriptableToggler<Rigidbody>
    {
        public override void ToggleComponent(Rigidbody component, bool enable)
        {
            component.isKinematic = !enable;
        }
    }
}