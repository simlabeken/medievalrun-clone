﻿using System;
using UnityEngine;
namespace ScriptableSystem
{
    [CreateAssetMenu(menuName = "ScriptableSystem/ScriptableBehaviour/ScriptableDebugger")]
    public class ScriptableDebugger : ScriptableObject
    {

        public void Log(GameObject go)
        {
            Log(go);
        }

        public void Log(ScriptableData value)
        {
            Log(value.ToString());
        }

        public void Log(string value)
        {
            Debug.Log(value);
        }

        public void LogWarning(GameObject go)
        {
            LogWarning(go);
        }
 
        public void LogWarning(ScriptableData value)
        {
            LogWarning(value.ToString());
        }

        public void LogWarning(string value)
        {
            Debug.LogWarning(value);
        }

        public void LogError(GameObject go)
        {
            LogError(go);
        }

        public void LogError(ScriptableData value)
        {
            Log(value.ToString());
        }

        public void LogError(string value)
        {
            Debug.LogError(value);
        }
    }


}