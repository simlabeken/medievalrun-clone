﻿using UnityEngine;
using UnityEngine.Events;

namespace ScriptableSystem.ScriptableBehaviour
{
    public abstract class ScriptableToggler<T> : ScriptableObject where T : Component
    {
        [SerializeField] bool toggleOn;
        [SerializeField] UnityEvent<GameObject> onCompleteEvent;


        public abstract void ToggleComponent(T component, bool enable);
        void ToggleComponents(T[] components, bool toggle)
        {
            foreach (var component in components)
            {
                ToggleComponent(component, toggle);
            }
        }

        public void Toggle(GameObject go)
        {
            Toggle(go, toggleOn);
        }

        public void Toggle(GameObject go, bool toggle)
        {
            var component = go.GetComponent<T>();
            ToggleComponent(component, toggle);
            onCompleteEvent?.Invoke(go);
        }

        public void ToggleAll(GameObject go)
        {
            ToggleAll(go, toggleOn);
        }

        public void ToggleAll(GameObject go, bool toggle)
        {
            var components = go.GetComponents<T>();
            ToggleComponents(components, toggle);
            onCompleteEvent?.Invoke(go);
        }

        public void ToggleInParent(GameObject go)
        {
            ToggleInParent(go, toggleOn);
        }

        public void ToggleInParent(GameObject go, bool toggle)
        {
            var component = go.GetComponentInParent<T>();
            ToggleComponent(component, toggle);
            onCompleteEvent?.Invoke(go);
        }

        public void ToggleAllInParent(GameObject go)
        {
            ToggleAllInParent(go, toggleOn);
        }

        public void ToggleAllInParent(GameObject go, bool toggle)
        {
            var components = go.GetComponentsInParent<T>();
            ToggleComponents(components, toggle);
            onCompleteEvent?.Invoke(go);
        }

        public void ToggleInChildren(GameObject go)
        {
            ToggleInChildren(go, toggleOn);
        }

        public void ToggleInChildren(GameObject go, bool toggle)
        {
            var component = go.GetComponentInChildren<T>();
            ToggleComponent(component, toggle);
            onCompleteEvent?.Invoke(go);
        }

        public void ToggleAllInChildren(GameObject go)
        {
            ToggleAllInChildren(go, toggleOn);
        }

        public void ToggleAllInChildren(GameObject go, bool toggle)
        {
            var components = go.GetComponentsInChildren<T>();
            ToggleComponents(components, toggle);
            onCompleteEvent?.Invoke(go);
        }

    }
}