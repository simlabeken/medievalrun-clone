﻿using UnityEngine;
using UnityEngine.Events;

namespace ScriptableSystem.ScriptableBehaviour
{
    [CreateAssetMenu(menuName = "ScriptableSystem/ScriptableBehaviour/ScriptableAnimationTriggerCaller")]
    public class ScriptableAnimationTriggerCaller : ScriptableObject
    {
        [SerializeField] StringReference trigger;
        [SerializeField] UnityEvent<GameObject> onTriggerCalled;

        public void CallTriggerInParent(GameObject go)
        {
            var animator = go.GetComponentInParent<Animator>();
            CallTrigger(animator);
        }

        public void CallTriggerInChildren(GameObject go)
        {
            var animator = go.GetComponentInChildren<Animator>();
            CallTrigger(animator);
        }

        public void CallTrigger(GameObject go)
        {
            var animator = go.GetComponent<Animator>();
            CallTrigger(animator);
        }

        void CallTrigger(Animator animator)
        {
            if (animator == null) return;

            animator.SetTrigger(trigger.Value);
            onTriggerCalled?.Invoke(animator.gameObject);
        }

    }
}

