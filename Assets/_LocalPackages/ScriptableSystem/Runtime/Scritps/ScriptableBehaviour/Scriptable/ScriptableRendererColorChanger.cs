﻿using Sirenix.OdinInspector;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace ScriptableSystem.ScriptableBehaviour
{

    [CreateAssetMenu(menuName = "ScriptableSystem/ScriptableBehaviour/ScriptableRendererColorChanger")]
    public class ScriptableRendererColorChanger : ScriptableObject 
    {
        [SerializeField] ColorReference color;
        [SerializeField] FloatReference delayTime;
        [SerializeField] UnityEvent<GameObject> onCompleteEvent;

        [Button]
        public void ChangeColor(GameObject go)
        {
            ChangeColor(go, color.Value,delayTime.Value);
        }

        [Button]
        public void ChangeColor(GameObject go, Color color,float delayTime)
        {
            MonoCoroutineCallerManager.CallCoroutine(IEChangeColorOfAnObject(go, color, delayTime));
        }

        public void ChangeColorAll(GameObject go)
        {
            ChangeColorAll(go, color.Value, delayTime.Value);
        }

        public void ChangeColorAll(GameObject go, Color color, float delayTime)
        {
            MonoCoroutineCallerManager.CallCoroutine(IEChangeColorOfAnObject(go, color, delayTime));
        }

        IEnumerator IEChangeColorAll(GameObject go, Color color, float delayTime)
        {
            foreach (var renderer in go.GetComponents<MeshRenderer>())
            {
                 MonoCoroutineCallerManager.CallCoroutine(IEChangeColor(renderer.gameObject, color, delayTime));
            }
            yield return new WaitForSecondsRealtime(delayTime);
            onCompleteEvent?.Invoke(go);
        }

        IEnumerator IEChangeColorOfAnObject(GameObject go, Color color, float delayTime)
        {
            MonoCoroutineCallerManager.CallCoroutine(IEChangeColor(go, color, delayTime));
            yield return new WaitForSecondsRealtime(delayTime);
            onCompleteEvent?.Invoke(go);
        }

        IEnumerator IEChangeColor(GameObject go, Color color, float delayTime)
        {
            var mat = go.GetComponent<MeshRenderer>().material;
            var oldColor = mat.color;
            mat.color = color;
            yield return new WaitForSecondsRealtime(delayTime);
            mat.color = oldColor;
        }
    }
}