using System;
using Sirenix.OdinInspector;

namespace ScriptableSystem
{
    [Serializable]
    [InlineProperty]
    public class StringReference //TODO: Add to project template
    {
        [HorizontalGroup("String Reference",MaxWidth = 100)]
        [ValueDropdown("valueList")]
        [HideLabel]
        public bool useValue = false;


        [HideIf("useValue", Animate = true)]
        [HorizontalGroup("String Reference")]
        [HideLabel]
        [Required]
        public ScriptableString variable;

        [ShowIf("useValue", Animate = true)]
        [HorizontalGroup("String Reference")]
        [HideLabel]
        public string constantValue;


        private ValueDropdownList<bool> valueList = new ValueDropdownList<bool>()
        {
            {"Reference",false },
            {"Value", true },
        };

        public string Value
        {
            get
            {
                return IsNullOrEmpty? "" : useValue ? constantValue : variable.GetValue();
            }
            set
            {
                if (useValue)
                    constantValue = value;
                else
                    variable.UpdateValue(value);
            }
        }


        public bool IsNullOrEmpty
        {
            get
            {
                return string.IsNullOrEmpty(useValue ? constantValue : variable ? variable.GetValue():"");
            }
        }
    }
}

