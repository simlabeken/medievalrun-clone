﻿using System;
using UnityEngine;


namespace ScriptableSystem
{
    [Serializable]
    public class FloatValueToggleable
    {
        public bool Enabled;
        [SerializeField] FloatReference floatValue;
        public float Value { get { return floatValue.Value; } }
    }
}