﻿using System;
using UnityEngine;


namespace ScriptableSystem
{
    [Serializable]
    public class IntValueToggleable
    {
        public bool Enabled;
        [SerializeField] IntReference intValue;
        public int Value { get { return intValue.Value; } }
    }
}