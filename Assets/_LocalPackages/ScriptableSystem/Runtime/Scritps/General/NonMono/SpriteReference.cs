﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace ScriptableSystem
{
    [Serializable]
    [InlineProperty]
    public class SpriteReference
    {
        [HorizontalGroup("Sprite Reference", MaxWidth = 100)]
        [ValueDropdown("valueList")]
        [HideLabel]
        public bool useValue = false;


        [HideIf("useValue", Animate = true)]
        [HorizontalGroup("Sprite Reference")]
        [HideLabel]
        [Required]
        public ScriptableSprite variable;

        [ShowIf("useValue", Animate = true)]
        [HorizontalGroup("Sprite Reference")]
        [HideLabel]
        public Sprite constantValue;


        private ValueDropdownList<bool> valueList = new ValueDropdownList<bool>()
        {
            {"Reference",false },
            {"Value", true },
        };

        public Sprite Value
        {
            get
            {
                //return IsNullOrEmpty? "" : useValue ? constantValue : variable.GetValue();
                return useValue ? constantValue : variable.GetValue();
            }
            set
            {
                if (useValue)
                    constantValue = value;
                else
                    variable.UpdateValue(value);
            }
        }

    }
}

