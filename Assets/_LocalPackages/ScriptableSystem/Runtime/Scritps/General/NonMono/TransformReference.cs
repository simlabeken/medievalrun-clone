﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace ScriptableSystem
{
    [Serializable]
    [InlineProperty]
    public class TransformReference
    {
        [HorizontalGroup("Transform Reference", MaxWidth = 100)]
        [ValueDropdown("valueList")]
        [HideLabel]
        public bool useValue = false;

        [HideIf("useValue", Animate = true)]
        [HorizontalGroup("Transform Reference")]
        [HideLabel]
        [Required]
        public ScriptableTransfom variable;

        [ShowIf("useValue", Animate = true)]
        [HorizontalGroup("Transform Reference")]
        [HideLabel]
        public Transform constantValue;


        private ValueDropdownList<bool> valueList = new ValueDropdownList<bool>()
        {
            {"Reference",false },
            {"Value", true },
        };

        public Transform Value
        {
            get
            {
                return useValue ? constantValue : variable.GetValue();
            }
            set
            {
                if (useValue)
                    constantValue = value;
                else
                    variable.UpdateValue(value);
            }
        }

    }
}

