﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace ScriptableSystem
{

    [Serializable]
    [InlineProperty]
    public class IntReference
    {
        [HorizontalGroup("Int Reference", MaxWidth = 100)]
        [ValueDropdown("valueList")]
        [HideLabel]
        public bool useValue = false;

        [HideIf("useValue", Animate = true)]
        [HorizontalGroup("Int Reference")]
        [HideLabel]
        [Required]
        public ScriptableInt variable;

        [ShowIf("useValue", Animate = true)]
        [HorizontalGroup("Int Reference")]
        [HideLabel]
        public int constantValue = 0;


        private ValueDropdownList<bool> valueList = new ValueDropdownList<bool>()
        {
            {"Reference",false },
            {"Value", true },
        };

        public int Value
        {
            get
            {
                return useValue ? constantValue : variable.GetValue();
            }
            set
            {
                if (useValue)
                    constantValue = value;
                else
                    variable.UpdateValue(value);
            }
        }

    }
}

