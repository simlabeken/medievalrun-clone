﻿using UnityEngine;
using UnityEngine.Events;

namespace ScriptableSystem
{
    //[CreateAssetMenu(fileName = "Vector3", menuName = "ScriptableSystem/ScriptableData/ScriptableVector3")]
    public class MonoVector3EventSetter : MonoSetter
    {
        [SerializeField] ScriptableVector3 vector3Data;
        [SerializeField] UnityEvent<Vector3> unityEvent;

        public override void Initialize()
        {
            UpdateValue();
            vector3Data.OnValueUpdated += UpdateValue;
        }

        public override void DeInitialize()
        {
            vector3Data.OnValueUpdated -= UpdateValue;
        }

        void UpdateValue()
        {
            unityEvent?.Invoke(vector3Data.GetValue());
        }
    }

}
