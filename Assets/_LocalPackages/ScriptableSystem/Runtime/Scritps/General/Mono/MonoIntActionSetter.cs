﻿using UnityEngine;
using UnityEngine.Events;

namespace ScriptableSystem
{
    public class MonoIntActionSetter : MonoSetter
    {
        [SerializeField] UnityEvent<int> action;
        [SerializeField] ScriptableIntAction scriptableAction;

        public override void DeInitialize()
        {
            scriptableAction.Action = null;
        }

        public override void Initialize()
        {
            scriptableAction.Action = action;
        }
    }

}
