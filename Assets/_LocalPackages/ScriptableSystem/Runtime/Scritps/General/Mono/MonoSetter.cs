﻿using UnityEngine;

namespace ScriptableSystem
{
    public abstract class MonoSetter : MonoBehaviour
    {
        [ReadOnly] public bool IsInitialized;
        public abstract void Initialize();
        public abstract void DeInitialize();

        private void Awake()
        {
            SetInitialize();
        }

        void SetInitialize()
        {
            if (IsInitialized) return;
                Initialize();
            IsInitialized = true;
        }

        void OnDestroy()
        {
            IsInitialized = false;
            DeInitialize();
        }
    }

}
