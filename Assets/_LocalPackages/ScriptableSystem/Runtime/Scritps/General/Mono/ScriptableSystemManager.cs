﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace ScriptableSystem
{
    public class ScriptableSystemManager : MonoBehaviourSingletonPersistent<ScriptableSystemManager>
    {
        // [RuntimeInitializeOnLoadMethod]
        [SerializeField] UnityEvent beforeMonoSetters;

        static void CreateMyself()
        {
            new GameObject("ScriptableSystemManager").AddComponent<ScriptableSystemManager>();
        }
        
        void Start()
        {
            SetMonoSetters();
            Invoke(nameof(SetMonoSetters),1);
        }

        private void OnEnable()
        {
            SceneManager.sceneLoaded += OnSceneLoaded;
        }

        private void OnSceneLoaded(Scene scene, LoadSceneMode loadSceneMode)
        {
            SetMonoSetters();
            Invoke(nameof(SetMonoSetters), 1);
        }

        [ContextMenu("Set Mono Setters")]
        void SetMonoSetters()
        {
            return;
            beforeMonoSetters?.Invoke();
            var monoSetters = FindObjectsOfType<MonoSetter>(true);
            foreach (var setter in monoSetters)
            {
                if (setter.IsInitialized) continue;
                    //setter.DeInitialize();
                setter.Initialize();
                setter.IsInitialized = true;
            }
        }
    }

}
