﻿using UnityEngine;
namespace ScriptableSystem
{
    [AddComponentMenu("LocalPackages/ScriptableSystem/MonoEnabler",0)]
    public class MonoEnabler : MonoSetter
    {
        [SerializeField] Behaviour behaviour;
        [SerializeField] ScriptableEnabler scriptableEnabler;
        [SerializeField] bool setOnStart;
        [SerializeField] bool enable;
        [SerializeField] float delayEnableTime;

        public override void DeInitialize()
        {
            scriptableEnabler.Activate -= SetActive;
            scriptableEnabler.Deactivate -= SetDeactive;
        }

        public override void Initialize()
        {
            if (scriptableEnabler == null) return;
            scriptableEnabler.Activate += SetActive;
            scriptableEnabler.Deactivate += SetDeactive;
            Invoke(nameof(ToggleActivate), delayEnableTime);
        }

        private void ToggleActivate()
        {
            if (!behaviour) return;
            if (setOnStart)
                behaviour.enabled= enable;
        }

        void SetActive()
        {
            behaviour.enabled = true;
        }

        void SetDeactive()
        {
            if (gameObject)
                behaviour.enabled = false;
        }
    }
}