﻿using UnityEngine;
using UnityEngine.Events;

namespace ScriptableSystem
{
    [AddComponentMenu("ScriptableSystem/MonoBoolEventListener")]
    public class MonoBoolEventListener : MonoSetter
    {
        [SerializeField] ScriptableBool boolData;
        [SerializeField] UnityEvent<bool> unityEvent;

        public override void Initialize()
        {
            UpdateValue();
            boolData.OnValueUpdated += UpdateValue;
        }

        public override void DeInitialize()
        {
            boolData.OnValueUpdated -= UpdateValue;
        }

        void UpdateValue()
        {
            unityEvent?.Invoke(boolData.GetValue());
        }
    }
}