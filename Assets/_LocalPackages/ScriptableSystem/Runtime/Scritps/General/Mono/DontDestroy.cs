using ScriptableSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestroy : MonoSetter
{


    public override void DeInitialize()
    {

    }


    public override void Initialize()
    {
        if (IsInitialized) return;
        DontDestroyOnLoad(gameObject);    
    }

}

