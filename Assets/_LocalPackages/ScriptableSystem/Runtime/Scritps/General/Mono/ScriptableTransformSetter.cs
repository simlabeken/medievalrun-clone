﻿using System;
using UnityEngine;

namespace ScriptableSystem
{
    [CreateAssetMenu(fileName = "TransformSetter ", menuName = "ScriptableSystem/ScriptableTransformSetter")]
    public class ScriptableTransformSetter : ScriptableObject
    {
        public Action<Transform> SetTransform;

        public void CallSetTransform(ScriptableTransfom anchor)
        {
            SetTransform?.Invoke(anchor.CurrentTransform);
        }
    }
}
