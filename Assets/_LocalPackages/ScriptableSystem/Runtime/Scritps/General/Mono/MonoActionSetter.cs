﻿using UnityEngine;
using UnityEngine.Events;

namespace ScriptableSystem
{
    public class MonoActionSetter : MonoSetter
    {
        [SerializeField] UnityEvent action;
        [SerializeField] ScriptableVoidAction scriptableAction;
        
        public override void DeInitialize()
        {
            scriptableAction.Action.RemoveListener(CallAction);
        }

        public override void Initialize()
        {
            scriptableAction.Action.AddListener(CallAction);
        }

        public void CallAction()
        {
            action?.Invoke();
        }
    }

}
