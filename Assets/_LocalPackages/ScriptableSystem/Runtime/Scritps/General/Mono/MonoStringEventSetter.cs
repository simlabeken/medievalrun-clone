﻿using UnityEngine;
using UnityEngine.Events;

namespace ScriptableSystem
{
    [AddComponentMenu("LocalPackages/ScriptableSystem/MonoDataEventSetter/MonoStringEventSetter",0)]
    public class MonoStringEventSetter : MonoDataEventSetter
    {
        [SerializeField] string prefix;
        [SerializeField] string suffix;
        [SerializeField] UnityEvent<string> unityEvent;

        protected override void UpdateValue()
        {
            unityEvent?.Invoke(prefix+data.Value.ToString()+ suffix);
        }
    }


}