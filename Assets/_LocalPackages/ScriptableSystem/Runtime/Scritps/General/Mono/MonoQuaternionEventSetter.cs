﻿using UnityEngine;
using UnityEngine.Events;

namespace ScriptableSystem
{
    [AddComponentMenu("LocalPackages/ScriptableSystem/MonoDataEventSetter/MonoQuaternionEventSetter",0)]
    public class MonoQuaternionEventSetter : MonoDataEventSetter
    {
        [SerializeField] UnityEvent<Quaternion> unityEvent;

        protected override void UpdateValue()
        {
            unityEvent?.Invoke((Quaternion)data.Value);
        }
    }


}
