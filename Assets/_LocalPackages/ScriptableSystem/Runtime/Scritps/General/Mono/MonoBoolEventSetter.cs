﻿using UnityEngine;
using UnityEngine.Events;

namespace ScriptableSystem
{
    [AddComponentMenu("LocalPackages/ScriptableSystem/MonoDataEventSetter/MonoBoolEventSetter",0)]
    public class MonoBoolEventSetter : MonoDataEventSetter
    {
        [SerializeField] UnityEvent<bool> unityEvent;

        protected override void UpdateValue()
        {
            unityEvent?.Invoke((bool)data.Value);
        }
    }


}
