﻿using UnityEngine;
namespace ScriptableSystem
{
    public class ScriptableManager<T> : ScriptableObject where T : Component
    {
        private T instance;
        public T Instance
        {
            get
            {
                if (instance == null)
                {
                    var objs = FindObjectsOfType(typeof(T)) as T[];
                    if (objs.Length > 0)
                        instance = objs[0];
                    if (objs.Length > 1)
                    {
                        Debug.LogError("There is more than one " + typeof(T).Name + " in the scene.");
                    }
                }
                return instance;
            }
        }
    }

}