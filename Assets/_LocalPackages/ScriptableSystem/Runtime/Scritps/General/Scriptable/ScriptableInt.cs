﻿using System;
using Newtonsoft.Json;
using Sirenix.OdinInspector;
using UnityEngine;

namespace ScriptableSystem
{
    [CreateAssetMenu(menuName = "ScriptableSystem/ScriptableData/ScriptableInt")]
    public class ScriptableInt : ScriptableData
    {
        [SerializeField] int value;
        public override object Value { get { return value; } protected set { this.value = (int)value; } }
        [Toggle("Enabled")]
        [SerializeField] IntValueToggleable minValue;
        [Toggle("Enabled")]
        [SerializeField] IntValueToggleable maxValue;
        public Action<int> OnValueIncreased;
        public Action<int> OnValueDecreased;

        public int GetValue() => (int)Value;


        public void IncreaseValue(ScriptableInt v)
        {
            IncreaseValue(v.value);
        }

        public void MultiplyValue(ScriptableInt v)
        {
            MultiplyValue(v.value);
        }

        public void DivideValue(ScriptableInt v)
        {
            DivideValue(v.value);
        }

        public void IncreaseValue(int v)
        {
            OnValueIncreased?.Invoke(v);
            UpdateValue(value + v);

        }

        public void DecreaseValue(ScriptableInt v)
        {
            DecreaseValue(v.value);
        }

        public void DecreaseValue(int v)
        {
            OnValueDecreased?.Invoke(v);
            UpdateValue(value - v);
        }

        public void MultiplyValue(int v)
        {
            OnValueIncreased?.Invoke(v);
            UpdateValue(value * v);
        }

        public void DivideValue(int v)
        {
            OnValueDecreased?.Invoke(v);
            UpdateValue(value / v);
        }

        public override void UpdateValue(object value, bool callUpdated = true)
        {
            if (minValue.Enabled)
                value = Math.Max(minValue.Value, (int)value);
            if (maxValue.Enabled)
                value = Math.Min(maxValue.Value, (int)value);
            base.UpdateValue(value);
        }

        public override void Load()
        {
            var loadData = JsonConvert.DeserializeObject<int>(PlayerPrefs.GetString(saveKey, ""));
            UpdateValue(loadData);
        }

    }
}