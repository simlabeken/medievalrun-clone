﻿using Sirenix.OdinInspector;
using System;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

namespace ScriptableSystem
{

    [CreateAssetMenu(fileName = "Action", menuName = "ScriptableSystem/ScriptableAction")]
    public class ScriptableAction : ScriptableObject
    {
        [ReadOnly] public UnityEvent Action;

        [ContextMenu("Call Action")]
        //[Button]
        public void CallAction()
        {
            Action?.Invoke();
        }
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(ScriptableAction))]
    public class ScriptableActionEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            ScriptableAction action = (ScriptableAction)target;
            if(GUILayout.Button("Call Action"))
            {
                action.CallAction();
            }
        }
    }
#endif

}