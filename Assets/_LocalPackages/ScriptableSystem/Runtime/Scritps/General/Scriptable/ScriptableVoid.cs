﻿using UnityEngine;

namespace ScriptableSystem
{
    [CreateAssetMenu(menuName = "ScriptableSystem/ScriptableData/ScriptableVoid")]
    public class ScriptableVoid : ScriptableData
    {
        public override object Value { get => throw new System.NotImplementedException(); protected set => throw new System.NotImplementedException(); }

        public override void Load()
        {
            throw new System.NotImplementedException();
        }
    }
}
