﻿using Newtonsoft.Json;
using UnityEngine;

namespace ScriptableSystem
{
    [CreateAssetMenu(menuName = "ScriptableSystem/ScriptableData/ScriptableAnimationCurve")]
    public class ScriptableAnimationCurve : ScriptableData
    {
        [SerializeField] AnimationCurve value;
        public override object Value { get { return value; } protected set { this.value = (AnimationCurve)value; } }
        public AnimationCurve GetValue() => (AnimationCurve)Value;

        public override void Load()
        {
            var loadData = JsonConvert.DeserializeObject<AnimationCurve>(PlayerPrefs.GetString(saveKey, ""));
            UpdateValue(loadData);
        }
    }
}
