﻿using Newtonsoft.Json;
using UnityEngine;

namespace ScriptableSystem
{
    [CreateAssetMenu(menuName = "ScriptableSystem/ScriptableData/ScriptableTransfom")]
    public class ScriptableTransfom : ScriptableData
    {
        [ReadOnly]public Transform CurrentTransform;

        public override object Value { get { return CurrentTransform; } protected set { CurrentTransform = (Transform)value; } }
        public Transform GetValue() => (Transform)Value;


        public void SetTransform(Transform transform)
        {
            Value = transform;
        }

        public override void Load()
        {
            var loadData = JsonConvert.DeserializeObject<Transform>(PlayerPrefs.GetString(saveKey, ""));
            UpdateValue(loadData);
        }
    }
}