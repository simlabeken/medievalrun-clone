using System;
using UnityEditor;
using UnityEngine;

namespace ScriptableSystem
{
    [CreateAssetMenu(fileName = "Activator", menuName = "ScriptableSystem/ScriptableActivator")]
    public class ScriptableActivator : ScriptableObject
    {
        public Action Activate;
        public Action Deactivate;

        [ContextMenu("Call Activate")]
        public void CallActivate()
        {
            Activate?.Invoke();
        }

        [ContextMenu("Call Deactivate")]
        public void CallDeactivate()
        {
            Deactivate?.Invoke();
        }
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(ScriptableActivator))]
    public class ScriptableActivatorEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            ScriptableActivator activator = (ScriptableActivator)target;
            if (GUILayout.Button("Call Activate"))
            {
                activator.CallActivate();
            }

            if (GUILayout.Button("Call Deactivate"))
            {
                activator.CallDeactivate();
            }
        }
    }
#endif
}
