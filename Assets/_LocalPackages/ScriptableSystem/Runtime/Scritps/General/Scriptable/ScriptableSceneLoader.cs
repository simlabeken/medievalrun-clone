﻿using UnityEngine;
using UnityEngine.SceneManagement;
namespace ScriptableSystem
{
    [CreateAssetMenu(fileName = "SceneLoader", menuName = "ScriptableSystem/SceneLoader")]
    public class ScriptableSceneLoader : ScriptableObject
    {
        public void LoadScene(int scene)
        {
            SceneManager.LoadScene(scene, LoadSceneMode.Single);
        }

        public void LoadSceneAdditive(int scene)
        {
            SceneManager.LoadScene(scene, LoadSceneMode.Additive);
        }
    }
}