﻿using UnityEngine;

namespace ScriptableSystem.ScriptableCondition
{
    public abstract class ScriptableIntCondition : ScriptableCondition
    {

        [SerializeField] protected ScriptableInt value;
        [SerializeField] protected IntReference comparaValue;

        void OnValidate()
        {
            OnConditionChanged?.Invoke();
            Debug.Log("Condition status changed");
        }

        public override void InitializeCondition()
        {
            value.OnValueUpdated += SetConditionProvided;
        }

        public override void DeinitializeCondition()
        {
            value.OnValueUpdated -= SetConditionProvided;
        }

        public abstract void SetConditionProvided();

    }
}