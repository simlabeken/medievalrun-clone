﻿using UnityEngine;

namespace ScriptableSystem.ScriptableCondition
{
    [CreateAssetMenu(menuName = "ScriptableSystem/ScriptableCondition/IntConditionEqual")]
    public class ScriptableIntConditionEqual : ScriptableIntCondition
    {
        public override void SetConditionProvided()
        {
            isConditionProvided = value.GetValue() == comparaValue.Value;
            OnConditionChanged?.Invoke();

        }
    }
}