﻿using System;
using UnityEngine;

namespace ScriptableSystem.ScriptableCondition
{
    public abstract class ScriptableCondition : ScriptableObject
    {
        public Action OnConditionChanged;
        [ReadOnly] [SerializeField] protected bool isConditionProvided;
        public bool IsConditionProvided { get { return isConditionProvided; } }

        public virtual void InitializeCondition() { }
        public virtual void DeinitializeCondition() { }

    }
}