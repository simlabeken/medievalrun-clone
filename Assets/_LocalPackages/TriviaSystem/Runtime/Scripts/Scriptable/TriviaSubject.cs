﻿using System;
using UnityEngine;

namespace TriviaSystem
{
    [CreateAssetMenu(menuName = "TriviaSystem/TriviaSubject")]
    public class TriviaSubject : ScriptableObject
    {
        public static Action<TriviaSubject> OnSubjectChanged;

        public string TriviaQuestion = "";
    }
}