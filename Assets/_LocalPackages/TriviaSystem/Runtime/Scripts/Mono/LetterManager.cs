﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace TriviaSystem
{

    public class LetterManager : MonoBehaviour
    {
        public UnityEvent<string> OnWordUpdated;
        public UnityEvent<Letter> OnLetterAdded;
        public UnityEvent<Letter> OnLetterRemoved;
        public float LastAddedTime;
        [SerializeField] TriviaLevelManager currentLevelManager;
        public TriviaLevelManager CurrentLevelManager { get { return currentLevelManager; } } 
        public List<string> CollectedWords=new List<string>();
        public List<Letter> Letters;

        public string GetCurrentWord
        { get
            {
                var word = "";
                for (int i = 0; i < Letters.Count; i++)
                {
                    word += Letters[i].Character;
                }
                return word;
            }
        }

        public void TryAdd(Letter letter)
        {
            if (IsNextAvailableChar(letter.Character))
                AddLetter(letter);
        }

        public void AddLetter(Letter letter)
        {
            Letters.Add(letter);
            OnLetterAdded?.Invoke(letter);
            OnWordUpdated?.Invoke(GetCurrentWord);
        }

        public void TryRemoveLast()
         {
            if(Letters.Count > 0)
            {
                OnLetterRemoved?.Invoke(Letters[Letters.Count - 1]);
                Letters.RemoveAt(Letters.Count - 1);
                OnWordUpdated?.Invoke(GetCurrentWord);
            }

         }

        public bool IsWordCollected() { return GetNextChars.Length==0; }

        public char[] GetNextChars { get{return currentLevelManager.GetNextChars(this); } }

        public bool IsNextAvailableChar(char character)
        {
            var nextChars = GetNextChars;
            for (int i = 0; i < nextChars.Length; i++)
            {
                if (character.CompareWithoutCaseSensitivity(nextChars[i]))
                    return true;
            }
            return false;
        }

        public bool IsWordLegit() { return currentLevelManager.IsLegit(GetCurrentWord); }
        public bool IsWordWhole() { return currentLevelManager.IsWholeWord(GetCurrentWord); }

        public void RemoveLetters()
        {
            Letters.Clear();
        }

        public void SetTriviaManager(TriviaLevelManager triviaLevelManager)
        {
             currentLevelManager=triviaLevelManager;
        }

    }

}