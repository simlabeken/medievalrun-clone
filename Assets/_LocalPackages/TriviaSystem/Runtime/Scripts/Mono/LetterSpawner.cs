using Sirenix.OdinInspector;
using UnityEngine;


namespace TriviaSystem
{

    public class LetterSpawner : MonoBehaviour
    {
        [SerializeField] Letter letterPrefab;
        [ReadOnly] [SerializeField] protected Letter spawnedLetter;

        [SerializeField] char character;
        public char Character { get { return character; } }
        [SerializeField] bool spawnOnStart;

        private void Start()
        {
            if (spawnOnStart)
                Spawn();
        }

        private void OnValidate()
        {
            SetNameAsCharacter();
        }

        public void SetCharacter(char character)
        {
            this.character = character;
        }

        public void Spawn(float delay)
        {
            Invoke(nameof(Spawn), delay);
        }

        public virtual void Spawn()
        {
            spawnedLetter = Instantiate(letterPrefab, transform.position, transform.rotation,transform);
            spawnedLetter.transform.parent = null;
            spawnedLetter.SetCharacter(character);
        }

        void OnDrawGizmos()
        {
            Gizmos.color = new Color(0, 0, 1, 0.5f);
            Gizmos.DrawCube(transform.position, new Vector3(1, 1, 1));
        }

        [Button(nameof(SetNameAsCharacter))]
        void SetNameAsCharacter()
        {
            name = char.ToUpper(character).ToString();
        }
    }

}