﻿using UnityEngine;

namespace TriviaSystem
{
    public class TriviaUI : MonoBehaviour
    {
        [SerializeField] TMPro.TextMeshProUGUI tmp;

        private void OnEnable()
        {
            TriviaSubject.OnSubjectChanged += SetUI;
        }

        private void OnDisable()
        {
            TriviaSubject.OnSubjectChanged -= SetUI;
        }

        public void SetUI(TriviaSubject triviaSubject)
        {
            SetQuestion(triviaSubject.TriviaQuestion);
        }

        public void SetQuestion(string question)
        {
            tmp.SetText(question);
        }
        
    }

}