using UnityEngine;
using UnityEngine.Events;

namespace GroupMovement
{
    public abstract class Node : MonoBehaviour
    {
        public UnityEvent OnNodeAdded;
        public UnityEvent OnNodeRemoved;
        public UnityEvent OnNodeDestroyed;
        public abstract NodeController NodeController { get; set; }

        public abstract bool RemoveNode(bool update = true);
        public abstract void DestroyNode(bool update = true);


    }
}

