﻿using System;
using System.Collections.Generic;
using System.Linq;
using GroupMovement.ChainMovementSystem;
using ScriptableSystem;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace GroupMovement
{
    public abstract class NodeController : MonoBehaviour
    {
        [SerializeField] protected IntReference maxNodeCount;
        [SerializeField] protected IntReference maxActiveNodeCount;
        [SerializeField] protected Transform nodeParent;
        public abstract Node PrefabNode { get; }
        public abstract List<Node> GetNodeList { get; }

        [SerializeField] protected bool initializeOnStart;
        public abstract int InitialNodeCount { get; }
        public abstract int NodeCount { get; }

        public UnityEvent OnNodeCountBelowZero;


        private void Start()
        {
            if (initializeOnStart)
                Initialize();
        }

        public abstract void Initialize();

        public Node GetLeader()
        {
            if (GetNodeList.Any())
                return GetNodeList[0];
            return null;
        }

        [Button]
        public void CreatePrefabNode(bool update = true)
        {
            CreateNode(PrefabNode, update);
        }

        [Button]
        public void CreateNode(Node node, bool update = true)
        {
            AddNode(Instantiate(node), update);
        }

        public abstract void AddNode(Node node, bool update = true);
        public abstract void RemoveNode(Node node, bool update = true);
        public abstract void DestroyNode(Node node, bool update = true);

        protected abstract void CreateInitialNodes();

        [Button]
        public abstract void RemoveLastNode(bool update = true);

        [Button]
        protected abstract void OnUpdated();

        

    }
}