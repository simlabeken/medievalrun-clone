using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkidmarkController : MonoBehaviour
{
    [SerializeField] TrailRenderer[] skidmarkRenderers;
    [SerializeField] GameObject[] tireSmokes;
    bool isSkidmark = false;
    [SerializeField] bool ContinousSkidmark = false;

    //find better solution

    private void Start()
    {
        skidmarkRenderers = GetComponentsInChildren<TrailRenderer>();
        if (ContinousSkidmark) StartTireMark();
    }

    private void Update()
    {
        if(SwerveInputManager.Instance.MoveFactorX != 0 && !isSkidmark)
        {
            StartSkidmarks();
        }
    }

    private void OnEnable()
    {
        SwerveInputManager.OnTouchReleased += StopSkidmarks;
    }

    private void OnDisable()
    {
        SwerveInputManager.OnTouchReleased -= StopSkidmarks;
    }

    public void StartSkidmarks()
    {
        Debug.Log("sk'dmark started");
        if (!isSkidmark)
        {
            if (!ContinousSkidmark)
            {
                StartTireMark();
            }
            for (int i = 0; i < tireSmokes.Length; i++)
            {
                tireSmokes[i].SetActive(true);
            }
            isSkidmark = true;
        }
    }

    public void StopSkidmarks()
    {
        if (isSkidmark)
        {
            if (!ContinousSkidmark)
            {
                for (int i = 0; i < skidmarkRenderers.Length; i++)
                {
                    skidmarkRenderers[i].emitting = false;
                }
            }
            for (int i = 0; i < tireSmokes.Length; i++)
            {
                tireSmokes[i].SetActive(false);
            }
            isSkidmark = false;
        }
    }

    void StartTireMark()
    {
        for (int i = 0; i < skidmarkRenderers.Length; i++)
        {
            skidmarkRenderers[i].emitting = true;
        }
    }

    void StopTireMark()
    {
        for (int i = 0; i < skidmarkRenderers.Length; i++)
        {
            skidmarkRenderers[i].emitting = false;
        }
    }

    public void StopAllSkidmarks()
    {
        StopTireMark();
        StopSkidmarks();
    }
}
