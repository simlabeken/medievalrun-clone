using System;
using UnityEngine;

[Serializable]
public class Vector2Toggleable
{
    public bool Enabled;
    public Vector2 Value;
}