using System.Collections;
using DG.Tweening;
using GameLevelSystem;
using UnityEngine;
using TMPro;
using UnityEngine.Events;
using UnityEngine.UI;
using GroupMovement.ChainMovementSystem;

public class GameUIManager : MonoBehaviourSingleton<GameUIManager>
{
    [SerializeField] GameObject gameOverPanel;
    [SerializeField] GameObject winPanel;
    [SerializeField] GameObject nextButton;
    [SerializeField] TMP_Text multiplierText;
    [SerializeField] TMP_Text levelText;
    [SerializeField] GameObject tutorialObject;
    [SerializeField] TMP_Text currencyEarnedText;
    [Header("Events")]
    [SerializeField] UnityEvent passedEvent;
    [SerializeField] UnityEvent multipliedEvent;
    [SerializeField] UnityEvent failedEvent;
    [SerializeField] UnityEvent onLevelStarted;

    private void OnEnable()
    {
        GameStateManager.OnGameLevelLoaded+= ResetUI;
        LevelController.OnLevelFailed += OnLevelFailed;
        LevelController.OnLevelPassed += OnLevelPassed;
        //LevelController.OnLevelReloadStarted += ResetUI;
        GameStateManager.OnGameLevelLoaded += ResetUI;
    }

    private void OnDisable() {
        GameStateManager.OnGameLevelLoaded -= ResetUI;
        LevelController.OnLevelFailed -= OnLevelFailed;
        LevelController.OnLevelPassed -= OnLevelPassed;
        //LevelController.OnLevelReloadStarted -= ResetUI;
        GameStateManager.OnGameLevelLoaded -= ResetUI;
    }

    void ResetUI()
    {
        levelText.SetText("Level " + (LevelManager.Instance.CurrentMapIndex));
        gameOverPanel.SetActive(false);
        winPanel.SetActive(false);
        levelText.gameObject.SetActive(!LevelManager.Instance.TutorialActive);
        tutorialObject.SetActive(true);
        nextButton.GetComponent<Button>().interactable = true;
        onLevelStarted?.Invoke();
    }
    

    [ContextMenu("On Level Failed")]
    void OnLevelFailed()
    {
        gameOverPanel.SetActive(true);
        levelText.gameObject.SetActive(false);
        failedEvent?.Invoke();
    }

    [ContextMenu("On Level Passed")]
    public void OnLevelPassed()
    {
        winPanel.SetActive(true);
        levelText.gameObject.SetActive(false);
        nextButton.SetActive(true);
        passedEvent?.Invoke();
    }

    public void SetLevelPassText(string multiplier, string candyEarned)
    {
        multiplierText.SetText("x" + multiplier);
        currencyEarnedText.SetText(candyEarned);
    }
}
