using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ChainMechanism : MonoSingleton<ChainMechanism>
{
    public List<GameObject> soldiers;
    public Transform playerRoot;

    [SerializeField] private float movementDelay = 0.25f;
    [SerializeField] private float moveOriginDelay = 0.7f;
    void Update()
    {
        if(Input.GetMouseButton(0))
        {
            MoveListElements();
        }
        if(Input.GetMouseButtonUp(0))
        {
            MoveOrigin();
        }
    }

    public void StackSoldiers(GameObject other, int index)
    {
        other.transform.parent = transform; //topladığımız objeler script'in bulunduğu objenin altında sıralansın
        if (index == 1)
        {
            Vector3 newPos1 = playerRoot.transform.localPosition;
            newPos1.z += 1;
            other.transform.localPosition = newPos1;
        }
        Vector3 newPos = soldiers[index].transform.localPosition;
        newPos.z += 1;

        other.transform.localPosition = newPos;

        soldiers.Add(other);
    }
    private void MoveListElements()
    {
        for(int i = 1; i < soldiers.Count; i++)
        {
            var pos = soldiers[i].transform.localPosition;
            pos.x = soldiers[i - 1].transform.localPosition.x;
            soldiers[i].transform.DOLocalMove(pos, movementDelay);
        }
    }

    private void MoveOrigin()
    {
        for(int i = 1; i < soldiers.Count; i++)
        {
            Vector3 pos = soldiers[i].transform.localPosition;
            pos.x = soldiers[0].transform.localPosition.x;
            soldiers[i].transform.DOLocalMove(pos, moveOriginDelay);
        }
    }
}
