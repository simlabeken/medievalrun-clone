using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collision : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag.Equals("Soldier"))
        {
            if(!ChainMechanism.Instance.soldiers.Contains(other.gameObject))
            {
                var col = other.gameObject.GetComponent<Collision>();
                if (col != null) return;

                other.gameObject.tag = "Player";
                other.gameObject.AddComponent<Collision>();

                ChainMechanism.instance.StackSoldiers(other.gameObject, ChainMechanism.Instance.soldiers.Count - 1);
            }
        }
    }
}
