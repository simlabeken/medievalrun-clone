using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerTranform : MonoBehaviour
{
    [SerializeField] private List<GameObject> playerLevels;
    [SerializeField] private TMP_Text level;

    private int index = 0;
    [SerializeField] private int healht = 1;
    public int Health => healht;
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag.Equals("LevelUp"))
        {
            if(index < 4)
            {
                playerLevels[index].SetActive(false);
                index++;
                healht++;
                playerLevels[index].SetActive(true);
            }
        }
        else if(other.gameObject.tag.Equals("LevelDown"))
        {
            if(index > 0)
            {
                playerLevels[index].SetActive(false);
                index--;
                healht--;
                playerLevels[index].SetActive(true);
            }
        }
        level.text = healht.ToString();

    }
}
