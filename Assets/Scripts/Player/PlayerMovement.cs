using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private Transform sideMovementRoot;
    [SerializeField] private float sideMovementSensitivity;
    [SerializeField] private float rightLimit, leftLimit;
 
    private Vector2 inputDrag;
    private Vector2 previousMousePos;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        HandleInput();
        HandleSideMovement();
    }

    private void HandleSideMovement()
    {
        var localPos = sideMovementRoot.localPosition;
        localPos += Vector3.right * inputDrag.x * sideMovementSensitivity;
        localPos.x = Mathf.Clamp(localPos.x, leftLimit, rightLimit);

        sideMovementRoot.localPosition = localPos;
    }

    private void HandleInput()
    {
        if(Input.GetMouseButtonDown(0))
        {
            previousMousePos = Input.mousePosition;        
        }
        if(Input.GetMouseButton(0))
        {
            var deltaMouse = (Vector2)Input.mousePosition - previousMousePos;
            inputDrag = deltaMouse;
            previousMousePos = Input.mousePosition;
        }
        else
        {
            inputDrag = Vector2.zero;
        }
    }
}
