using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController : MonoSingleton<AnimationController>
{
    [SerializeField] private Animator animator;
    public void Idle()
    {
        animator.SetTrigger("Idle");
    }

    public void Run()
    {
        animator.SetTrigger("Run");
    }

}
