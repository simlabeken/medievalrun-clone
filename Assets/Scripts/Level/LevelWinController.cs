using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelWinController : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Finish")
        {
            GameLevelSystem.LevelController.OnLevelPassed?.Invoke();
            Time.timeScale = 0;
        }
    }
}
