using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SideAxe : MonoBehaviour
{
    [SerializeField] private GameObject soldiers;
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag.Equals("Player"))
        {
            ChangePosition(other.gameObject);
        }
    }
    private void ChangePosition(GameObject other)
    {
        //Debug.Log(other.transform.localPosition);
        List<GameObject> newList = new List<GameObject>();
        var index = ChainMechanism.Instance.soldiers.IndexOf(other.gameObject);
        var soldierCount = ChainMechanism.Instance.soldiers.Count;
        if (index > 0)
        {
            for (int i = index; i < soldierCount; i++)
            {
                var soldier = ChainMechanism.Instance.soldiers[i];
                newList.Add(soldier);

            }
            for (int i = 0; i < newList.Count; i++)
            {
                var soldier = newList[i];
                ChainMechanism.Instance.soldiers.Remove(soldier);
                soldier.tag = "Soldier";
                var currentpos = soldier.transform.position;
                var newpos = new Vector3(Random.Range(-3.2f, 3.3f), 0,
                                         Random.Range(currentpos.z + 8, currentpos.z + 12f));
                currentpos = newpos;
                soldier.transform.DOKill();
                soldier.transform.parent = null;
                soldier.transform.DOJump(currentpos, 5, 1, 0.5f);
                soldier.transform.position = currentpos;
                Debug.Log("Calculated pos: " + currentpos + " Transform pos: " + soldier.transform.position);
                Destroy(soldier.GetComponent<Collision>());

            }
        }
    }
}
