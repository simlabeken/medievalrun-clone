using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class EnemyDestroy : MonoBehaviour
{
    [SerializeField] private PlayerTranform playerTranform;
    [SerializeField] private TMP_Text hP;

    [SerializeField] private int health;
    private void Start() 
    {
        hP.text = health.ToString();
    }

    private void OnTriggerEnter(Collider other)
    {
        playerTranform = other.gameObject.GetComponent<PlayerTranform>();
        var playerHealth = playerTranform.Health;

        if (other.gameObject.tag.Equals("Player"))
        {
            var soldierHealth = health;
            health -= playerHealth;
            playerHealth -= soldierHealth;
            hP.text = health.ToString();

            if(health <= 0)
            {
                Destroy(gameObject);
            }

            if(playerHealth <= 0)
            {
                var index = ChainMechanism.Instance.soldiers.Count - 1;

                Destroy(ChainMechanism.Instance.soldiers[index]);
                ChainMechanism.Instance.soldiers.RemoveAt(index);
            }
        }
    }
}
